#include <stdio.h>
#include <string.h>

// page 126
int findindex(char string[], char letter){
    int i = 0;
    int max = strlen(string);
    for(i=0; i<max; i++){
        if(string[i] == letter){
            return i;
        }
    }
    return -1;
}

int main() {
    char key[11];
    scanf("%s", key);

    char word[20];
    while(strcmp(word, "-1\0")!=0){
        scanf("%s", word);
        int i, keyindex = 0;
        int keylen = strlen(key);
        char alphabet[] = "abcdefghijklmnopqrstuvwxyz";
        for(i=0; i< strlen(word); i++){
            printf("%c",97+((word[i]+findindex(alphabet, key[keyindex]))%97)%26);
            if(keyindex < keylen-1){
                keyindex++;
            }
            else{
                keyindex = 0;
            }
        }
        printf("\n");
    }
    return 0;
}
