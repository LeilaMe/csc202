#include <stdio.h>
#include <string.h>

int main() {
    char word[50];
    char key[20];
    scanf("%s", key);
    scanf("%s", word);
    int keylen = strlen(key);

    while(strcmp(word, "-1") != 0){
        int i, keyindex = 0;
        for(i=0; i< strlen(word); i++){
            printf("%c",'a'+((word[i]+key[keyindex]-'a')%'a'%26));
            if(keyindex < keylen-1){
                keyindex++;
            }
            else{
                keyindex = 0;
            }
        }
        printf("\n");
        scanf("%s", word);
    }
    return 0;
}
