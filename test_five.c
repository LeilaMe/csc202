#include <stdio.h>
#include "minunit.h"
#include "five.c"

int tests_run = 0;

int a[] = {2,3,0,9};
int b[] = {1,2,3};

static char * test_five_01() {
	mu_assert("error, five([2,3,0,9],4) != 9032", five(a,4) == 9032);
	return 0;
}

static char * test_five_02() {
        mu_assert("error, five([1,2,3],3) != 321", five(b,3) == 321);
        return 0;
}

static char * all_tests() {
	mu_run_test(test_five_01);
	mu_run_test(test_five_02);
	return 0;
}

int main(int argc, char **argv) {
	char *result = all_tests();
	if (result != 0) {
		printf("%s\n", result);
	}
	else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);
	return result != 0;
}
