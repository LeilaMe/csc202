#include <stdio.h>

int main() 
{
	int arr[5][5] = {{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5}};

	int i,j;
	printf("\n");
	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
			printf("\t%d", arr[i][j]);
		printf("\n");
	}
// reminder to use pointers
	int *ptr = 0;

	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			if (i == j)
				ptr += arr[i][j];
		}
	}
	printf("\n%d", *ptr);

	return 0;
}
