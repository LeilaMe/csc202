#include <stdio.h>
#include <stdlib.h>

int main() 
{
    int MARKS[20][5]; //stores marks from 20 students in 5 subjects
    int i,j; // i student, j subject
    int num = 1;
    for(i=0; i<20; i++)
    {
        printf("\n");
        for(j=0; j<5; j++)
        {
            MARKS[i][j] = num;
            num++;
            printf("%d\t", MARKS[i][j]);
        }
    }

    //avg_subject
    int avg_subject[5] = {0,0,0,0,0};
    for(j=0; j<5; j++)
    {
        for(i=0; i<20; i++) 
            avg_subject[j] = avg_subject[j] + MARKS[i][j];
    avg_subject[j] /= 20; // divide each element by number of students
    printf("\nThe average marks for subject %d is %d", j+1, avg_subject[j]);
    }

    //avg_student
    int *avg_student;
    avg_student = (int*)calloc(20, sizeof(int));
    for(i=0; i<20; i++)
    {
        for(j=0; j<5; j++)
            avg_student[i] += MARKS[i][j];
    avg_student[i] /= 5; // divide each element by number of subjects
    printf("\nThe average marks for student %d is %d", i+1, avg_student[i]);
    }

// students with average below 50
    num = 0;
    // printf("num is now %d.\n", num);
    for(i=0; i<20; i++)
    {
        if (avg_student[i] < 50)
            num++;
    }
    printf("\nThe number of students who have scored below 50 in their average is %d", num);

    return 0;
}
