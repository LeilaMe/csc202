#include <stdio.h>

int main()
{
	int i, j, row, col;
	printf("\nnumber of rows: ");
	scanf("%d", &row);
	printf("\nnumber of columns: ");
	scanf("%d", &col);

	int eggs[row][col];
	for(i = 0; i < row; i ++)
	{
		for(j = 0; j < col; j++)
		{
			printf("\n eggs[%d][%d] = ", i, j);
			scanf("%d", &eggs[i][j]);
		}
	}
	printf("\n The eggs matrix is: \n");
	for(i = 0; i < row; i ++)
	{
		for(j = 0; j < col; j++)
			printf("\t %d", eggs[i][j]);
		printf("\n");
	}
	
	int sum = 0;
	for(i = 0; i < row; i++)
	{
		for(j = 0; j < col; j++)
		{
			if (i == j)
				sum += eggs[i][j];
		}
	}
	printf("\n%d", sum);

	return 0;
}
