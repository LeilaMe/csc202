#include <stdio.h>

int main()
{
	int two_d_array[2][3]={{2,0,4},{9,0,1}};
	int i,j;
	int count = 0;

	for(i = 0; i < 2; i++)
	{
		for(j = 0; j < 3; j++)
		{
			if (two_d_array[i][j] != 0)
				count++;
		}
	}
	printf("\n%d", count);
	return 0;
}
