#include <stdio.h>

int main()
{
	int SIZE = 6;
	int my_array[6] = {0, 3, 6, 2, 1, 9};
	int i;

	printf("\n");
	for(i=0; i<SIZE; i++)
		printf("\t%d", my_array[i]);

	int second, secondLast;
	second = my_array[1];
	secondLast = my_array[SIZE-2];
	my_array[1] = secondLast;
	my_array[SIZE-2] = second;

	printf("\n");
	for(i=0; i<SIZE; i++)
		printf("\t%d", my_array[i]);

	return 0;
}
