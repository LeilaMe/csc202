Year = 2
E = 100
P = 50
R = 9

E_female = E
E_male = E
R_female = 0
R_male = 0

def check_even(num):
    if num % 2 == 0:
        return True
    else:
        return False

def find_R(f, m):
    if check_even(f):
        R_female = f / 2
        R_male = m / 2
    else:
        R_female = f / 2 + 0.5
        R_male = m // 2
    return R_female, R_male

for Y in range(Year):
    print("E_female+E_male")
    print(E_female+E_male)
    # winter
    E_female = E_female * P * 0.01
    E_male = E_male * P * 0.01
    print("E_female, E_male, Y, P")
    print(E_female, E_male, Y, P)
    #new births spring
    R_female, R_male = find_R(R, R)
    E_female += R_female
    E_male += R_male
    print("R_female, R_male")
    print(R_female, R_male)
    R_female, R_male = find_R(E_female, E_male)
    print("R_female, R_male")
    print(R_female, R_male)
    #output


