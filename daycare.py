length = int(input())
numbers = [i for i in input().split()]

for child in numbers:
    if numbers.count(child) == 1:
        print(child)
