#include <stdio.h>
#include "minunit.h"
#include "one.c"

int tests_run = 0;

int a[] = {5,1,2,3,4,5};
int b[] = {2,1,2};

static char * test_one_01() {
        mu_assert("error, one([5,1,2,3,4,5],6) != ", one(a,6) == "The array elements are 1 2 3 4 5");
        return 0;
}

static char * test_one_02() {
        mu_assert("error, one([2,1,2],2) != ", one(b,2) == "The array elements are 1 2");
        return 0;
}

static char * all_tests() {
        mu_run_test(test_one_01);
        mu_run_test(test_one_02);
	return 0;
}

int main(int argc, char **argv) {
        char *result = all_tests();
        if (result != 0) {
                printf("%s\n", result);
        }
        else {
                printf("ALL TESTS PASSED\n");
        }
        printf("Tests run: %d\n", tests_run);
        return result != 0;
}
