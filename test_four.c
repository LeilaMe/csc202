#include <stdio.h>
#include "minunit.h"
#include "four.c"

int tests_run = 0;

int a[] = {72,1,2,3,4};
int b[] = {1,2,3};

static char * test_four_01() {
    mu_assert("error, four([72,1,2,3,4],5) != 4", four(a,5) == 4);
    return 0;
}

static char * test_four_02() {
    mu_assert("error, four([1,2,3],3) != 2", four(b,3) == 2);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_four_01);
    mu_run_test(test_four_02);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);
    return result != 0;
}
