fraction = [int(num) for num in input().split()]
while fraction[0] != -1:
    n = fraction[0] 
    d = fraction[1]

    i = 2
    while d%n != 0:
        if n * i - d > 0:
            n = n * i - d
            d = d * i
            print(i)
        i += 1
    print(d//n)
    fraction = [int(num) for num in input().split()]
    if fraction[0] != -1:
        print("x-x-x-x-x-x")
