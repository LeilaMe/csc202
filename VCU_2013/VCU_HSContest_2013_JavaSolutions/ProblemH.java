import java.io.*;
public class ProblemH 
{
static DataInputStream input;
static long MaxCommonDivisor(long a,long b)
{
while (a%b !=0 )
    {
     long c=a%b;
     a=b;
     b=c;
     }
  return b; 
 }
static long FindNextNumber (long T, long B, long prevnum)
{
long retval=prevnum+1;
for (;1.0/retval>=(T*1.0/B);retval++);
return retval;
}
static void Egyptian(long T, long B, long prevnum)
{
long retval=0;
if (T==1)  {retval=B; 
    System.out.println(retval);
  }
else
{
retval= FindNextNumber(T,B,prevnum);
System.out.println(retval);
T=T*retval-B;
B=B*retval;
long temp=MaxCommonDivisor(B, T);
T=T/temp;
B=B/temp;
Egyptian(T,B,retval);
}
}
public static void main(String[] args) 
{
try {
       input = new DataInputStream(new BufferedInputStream(System.in));
        String line;
        int i=0;
        while ((line = input.readLine()) != null)
           {
			   if (i>0)
					System.out.println("x-x-x-x-x-x");
					i++;
            String[] buffer=line.split(" ");
            long num1= Long.parseLong(buffer[0]);
            long num2= Long.parseLong(buffer[1]);
            Egyptian(num1,num2, 1);
            }
       } 
              catch (IOException ex) {System.out.println(ex);}
              } // main
}//class
