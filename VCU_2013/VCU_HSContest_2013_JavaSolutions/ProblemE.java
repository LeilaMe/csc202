import java.util.*;
public class ProblemE
{

	public static void main(String args[]) throws Exception
	{
		int rounds;
		int nEmployees;
		int longestChain;
		int i;
		double maxBinaryTreeHeight;
		double edgeCnt;
		double avgTime;

		Scanner cin = new Scanner(System.in);
		rounds = cin.nextInt();

                for (i=1;i<=rounds;i++)
                {
                  nEmployees=cin.nextInt();
                  maxBinaryTreeHeight=Math.ceil(Math.log(nEmployees+1)/Math.log(2));
                  edgeCnt=maxBinaryTreeHeight-1;
                  avgTime=14.35*edgeCnt;
                  longestChain=(int)Math.round(avgTime);
                  System.out.println(longestChain);
		}
	}	
}
