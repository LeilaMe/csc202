#notes: ordered by when they bought the ticket
#max 100,000 tickets sold
#3 winners chosen from the list
#
# terminal: python3 Problem_D.py < inputD.txt

num_tickets = int(input())
print(num_tickets)
numbers = [int(num) for num in input().split()]

minLeft = [0] * num_tickets
maxRight = [0] * num_tickets

for indx in range(num_tickets):
    numMin = 0
    numMax = 0
# counts the number of tickets before it that are smaller
    for i in range(indx):
        if numbers[i] < numbers[indx]:
            numMin += 1
        minLeft[indx] = numMin
# counts the number of tickets after it that are bigger
    for i in range(indx, num_tickets):
        if numbers[i] > numbers[indx]:
            numMax += 1
        maxRight[indx] = numMax

solution = 0
for i in range(num_tickets):
    solution += (minLeft[i] * maxRight[i])
print(minLeft)
print(maxRight)
print(solution)
