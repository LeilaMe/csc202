#include <stdio.h>
#include <stdlib.h>

struct child
{
    int fav_num;
    char name[20];
    char fav_color[20];
};
void even(struct child *);

int main()
{
    struct child *ptr_ch;
    ptr_ch = (struct child *)malloc(sizeof(struct child));
    ptr_ch->fav_num = 72;

    struct child *ptr_ch2;
    ptr_ch2 = (struct child *)malloc(sizeof(struct child));
    ptr_ch2->fav_num = 7;

    even(ptr_ch);
    even(ptr_ch2);
    return 0;
}
void even(struct child *ptr)
{
    if(ptr->fav_num % 2 == 0)
    {
        printf("\n%d is even", ptr->fav_num);
    }else{
        printf("\n%d is odd", ptr->fav_num);
    }
}
