#include <stdio.h>

// my recipe (declare structure)
typedef struct
{
    int angle_one;
    int angle_two;
    int angle_three;
}triangle;

// define my function
void is_triangle(triangle);

int main()
{
    // initialize the triangles
    triangle tri1 = {45, 45, 90};
    triangle tri2 = {10, 10, 10};
    
    is_triangle(tri1);
    is_triangle(tri2);
    
    return 0;
}
void is_triangle(triangle tri)
{
    if(tri.angle_one + tri.angle_two + tri.angle_three == 180){
        printf("\nyes. %d + %d + %d = 180", tri.angle_one, tri.angle_two, tri.angle_three);
    }else{
        printf("\nno. %d + %d + %d != 180", tri.angle_one, tri.angle_two, tri.angle_three);
    }
}

