#include <stdio.h>

// my recipe (declare structure)
typedef struct
{
    int angle_one;
    int angle_two;
    int angle_three;
}triangle;

// define my function
void is_triangle(int, int, int);

int main()
{
    // initialize the triangles
    triangle tri1 = {45, 45, 90};
    triangle tri2 = {10, 10, 10};
    
    is_triangle(tri1.angle_one, tri1.angle_two, tri1.angle_three);
    is_triangle(tri2.angle_one, tri2.angle_two, tri2.angle_three);
    
    return 0;
}
void is_triangle(int ang1, int ang2, int ang3)
{
    if(ang1 + ang2 + ang3 == 180){
        printf("\nyes. %d + %d + %d = 180", ang1, ang2, ang3);
    }else{
        printf("\nno. %d + %d + %d != 180", ang1, ang2, ang3);
    }
}

