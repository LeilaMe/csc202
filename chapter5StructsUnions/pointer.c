#include <stdio.h>

struct flower
{
    int stem_length;
    int num_petals;
    int num_leaves;
    int root_length;
};

int main()
{
    struct flower *ptr_flwr, flwr; // declare pointer variable
    //order doesn't matter
    ptr_flwr = &flwr; // assign the address

    // parentheses
    (*ptr_flwr).stem_length = 12;
    (*ptr_flwr).num_petals = 8;
    // pointing to operator ->
    ptr_flwr->num_leaves = 6;
    ptr_flwr -> root_length = 400;

    printf("\nstem length: %d", ptr_flwr->stem_length);
    printf("\nnum petals: %d", ptr_flwr->num_petals);
    printf("\nnum leaves: %d", ptr_flwr->num_leaves);
    printf("\nroot length: %d", ptr_flwr->root_length);
}

