#include <stdio.h>
#include <string.h>

#define NUM_BIRDS 2

int main()
{
    struct bird // struct name
    {
        char name[20]; // three members
        int wingspan;
        char date_spotted[20];
    };
    //struct bird chirp_things[NUM_BIRDS]; // array of structs with index 10
/*
    strcpy(chirp_things[0].name, "sweet pea");
    chirp_things[0].wingspan = 10;
    strcpy(chirp_things[0].date_spotted, "10 23 2003");

    strcpy(chirp_things[1].name, "mustard");
    chirp_things[1].wingspan = 300;
    strcpy(chirp_things[1].date_spotted, "11 11 2011");
*/
//also can initialize array of struct variables at declaration
    struct bird chirp_things[NUM_BIRDS] = {{"peachy", 4, "01 13 1999"}, {"bob", 5, "03 14 1592"}};

    int i;

    for(i=0; i<NUM_BIRDS; i++)
    {
        printf("\nname: %s", chirp_things[i].name);
        printf("\nwingspan: %d", chirp_things[i].wingspan);
        printf("\ndate spotted: %s", chirp_things[i].date_spotted);
    }
}

