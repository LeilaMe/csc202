#include <stdio.h>

typedef struct{
    int day, month, year;
}DATE;

int valid(int, int, int);

int main() {
    // get date
    DATE date;
    printf("enter day\n");
    scanf("%d", &date.day);
    printf("enter month\n");
    scanf("%d", &date.month);
    printf("enter year\n");
    scanf("%d", &date.year);

    int validoutput;
    validoutput = valid(date.day, date.month, date.year);
    if(validoutput == 0 ){
        printf("it is valid\n");
        printf("%d %d %d\n", date.day, date.month, date.year);
    }else{
        printf("it is not valid\n");
    }

    /*valid(100, 1, 2000);
    valid(1, 100, 2000);
    valid(0, 1, 2000);
    valid(1, 0, 2000);
    valid(29, 2, 2010);
    valid(31, 6, 2007);
    valid(30, 2, 2000);
    valid(32, 10, 2000);

    valid(29, 2, 2020);
    valid(1, 1, 2000);
    valid(31, 1, 2000); */
}

int valid(int day, int month, int year){
    if(month == 2 && day == 29 && (year%4) == 0)
    {
        //printf("valid\n");
        return 0;
    }else if(day>31 || month>12 || month<1 || day<1){
        //printf("not valid\n");
        return 1;
    }else if(day==31 && (month==4 || month==6 || month==9 || month==11)){
        //printf("not valid\n");
        return 1;
    }else if(month == 2 && day>28){
        //printf("not valid\n");
        return 1;
    }else{
        //printf("valid\n");
        return 0;
    }
}
