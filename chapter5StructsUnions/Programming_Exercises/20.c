#include <stdio.h>

/* Declare a structure fraction that has two fields- numerator and denominator.
   Create two variables and compare them using function. Return 0 if the two
   fractions are equal, -1 if the first fraction is less than the second and 1
   otherwise. You may convert a fraction into a floating point number for your
   convenience. */

struct fraction{
    float numerator, denominator;
};
int compare_frac(struct fraction, struct fraction);

int main()
{
    struct fraction f_one = {2, 3};
    struct fraction f_two = {10, 15};
    
    printf("%d\n",compare_frac(f_one, f_two)); //passing variables

    struct fraction f_three = {1, 2};
    printf("%d\n", compare_frac(f_three, f_one));
    printf("%d\n", compare_frac(f_one, f_three));

    return 0;
}
int compare_frac(struct fraction one, struct fraction two)
{
    float float_one, float_two;
    float_one = one.numerator / one.denominator; 
    printf("float one: %f\n", float_one);
    float_two = two.numerator / two.denominator;
    printf("float two: %f\n", float_two);

    if(float_one == float_two){
        printf("%f = %f\n", float_one, float_two);
        return 0;
    }
    else if(float_one < float_two){
        return -1;
    }else {
        return 1;
    }
}
