#include <stdio.h>

/* Write a program to define a union and a structure both having exactly the
   same members. Using the sizeof operator, print the size of structure variable
   as well as union variable and comment on the result */

int main()
{
    struct a_struct{
        int number;
        char word[20];
        float decimal;
    };

    struct a_struct astr;

    union a_union{
        int number;
        char word[20];
        float decimal;
    };

    union a_union auni;

    printf("This is the size of struct variable: %d\n", sizeof(astr));
    printf("This is the size of union variable: %d\n", sizeof(auni));

    printf("it outputted 28 for struct and 20 for the union.\n");
    printf("the size of the union with only int: 4\n");
    printf("the size of the union with only char: 20\n"); 
    printf("the size of the union with only float: 4\n");
    printf("the size of the union with int and float: 4\n");
    printf("the size of the struct with int and float: 8\n");
    printf("the size of the union is the size of the largest member\n");
    printf("the size of the struct is the sum of the sizes of the members\n");

    return 0;
}
