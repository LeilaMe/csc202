#include <stdio.h>

struct simplevector{
    float change_x, change_y;
};

struct anglevector{
    float angle;
    float magnitude;
};

void addsimple(struct simplevector one, struct simplevector two)
{
    printf("change x: %f and change y: %f\n",one.change_x+two.change_x, one.change_y+two.change_y);
}
void scalesimple(struct simplevector one)
{
    printf("old:\nx: %f y: %f\nnew:\nx: %f y: %f\n", one.change_x,one.change_y,one.change_x*10,one.change_y*10);
}

/*
void addangle(struct anglevector one, struct anglevector two)
{
    //printf("new angle: %f\n",(one.angle + two.angle)/2);
    // https://en.vcenter.ir/technical/vector-definition/
    // idk
}
*/
int main()
{
    int num_vectors;
    scanf("%d", &num_vectors); // gets number of vectors

    struct simplevector vectors[num_vectors]; // declare array of vectors

    int i;
    for(i=0; i<num_vectors; i++)
    {
        scanf("%f",&vectors[i].change_x); 
        scanf("%f",&vectors[i].change_y); 
    }

    addsimple(vectors[0], vectors[1]);
    scalesimple(vectors[0]);

    /*
    struct anglevector vects[num_vectors];

    int i;
    for(i=0; i<num_vectors; i++)
    {
        scanf("%f", &vects[i].angle);
        scanf("%f", &vects[i].magnitude);
    }
*/
}
