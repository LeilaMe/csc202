#include <stdio.h>
#include <stdlib.h>
struct farm
{
    int num_cows;
    int num_pigs;
    int num_chickens;
    int num_elephants;
};

struct farm *ptr_frm[3];

int main()
{
    ptr_frm[0] = (struct farm *)malloc(sizeof(struct farm));
    ptr_frm[1] = (struct farm *)malloc(sizeof(struct farm));
    ptr_frm[2] = (struct farm *)malloc(sizeof(struct farm));

    ptr_frm[0]->num_cows = 2;

    int i;
    for(i = 0; i < 3; i++)
    {
        printf("\nfarm #%d: %d cows", i+1, ptr_frm[i]->num_cows);
    }
}
