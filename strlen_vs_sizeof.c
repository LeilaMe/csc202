#include <stdio.h>
#include <string.h>
int main() {
    char str1[] = "hi.";
    char str2[] = {'h', 'i', '.'}; // no null terminator

    int size1=0, size2=0, len1 = 0, len2 = 0;
    size1 = sizeof(str1);
    size2 = sizeof(str2);
    len1 = strlen(str1);
    len2 = strlen(str2);

    printf("\n%d is sizeof(str1)",size1);
    printf("\n%d is sizeof(str2)",size2);
    printf("\n%d is strlen(str1)",len1);
    printf("\n%d is strlen(str2)",len2);

    return 0;
}
