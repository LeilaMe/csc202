#include <stdio.h>
#define MAX 100

int main()
{
    // for number of test cases
    int num_cases, c;
    char newline;
    scanf("%d", &num_cases);
    scanf("%c", &newline);
    for(c=0;c<num_cases;c++)
    {
        int FLAG = 0; // zero means nothing wrong
        printf("case number %d:\n", c+1);
        // first get user input into a stack and ignore spaces
        char ch, expression[MAX], close_stack[MAX];
        int top = -1, ctop = -1;
        ch = getchar();
//        printf("char: %c\n",ch);
        while(ch != '\n')
        {
            top++;
            expression[top] = ch;
            ch = getchar();
        }
        // display
        int i;
/*        for(i=top;i>=0;i--)
            printf("%c\n",expression[i]);
*/
        int temp = top;
        for(i=temp;i>=0;i--)
        {
            //printf("expression[i]: %c, i: %d\n", expression[i], i);
            //printf("top: %d, ctop: %d\n", top, ctop);
            top--;
            if(expression[i] == ')' || expression[i] == '}' || expression[i] == ']') // pop and push
            {
                ctop++;
                close_stack[ctop] = expression[i];
            }
            else if(expression[i] == '(' || expression[i] == '{' || expression[i] == '[')
            {
                if(ctop == -1) // empty
                {
                    FLAG++;
                    printf("too many or wrong order (, {, or [\n");
                }
                else{ // check and then pop both
                    if(expression[i] == '(' && close_stack[ctop] == ')')
                        ctop--;
                    else if(expression[i] == '{' && close_stack[ctop] == '}')
                        ctop--;
                    else if(expression[i] == '[' && close_stack[ctop] == ']')
                        ctop--;
                    else{
                        FLAG++;
                        printf("too many or wrong order (, {, or [\n");
                    }
                }
            }
        }
        /*
        printf("what's left in the expression\n");
        for(i=top;i>=0;i--)
            printf("%c\n", expression[i]);
        printf("what's left in the close stack\n");
        for(i=ctop;i>=0;i--)
            printf("%c\n", close_stack[i]);
*/
        if(ctop != -1)
        {
            FLAG++;
            printf("too many or wrong order ), }, or ]\n");
        }

        if(FLAG == 0)
            printf("all good\n");
        else
            printf("this many problems: %d\n", FLAG);
            
    }

    return 0;
}

