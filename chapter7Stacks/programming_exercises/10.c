#include <stdio.h>
#define MAX 30

//write a program to reverse a string using recursion

char stack[MAX], letter; 
int top = -1;

void displayword(int top)
{
    int i;
    for(i = top; i >= 0; i--)
        printf("%c\n", stack[i]);
}
char[] revstring()
{
    char lastletter, output[MAX];
    int temp = top;
    if(temp==0)
        return stack[0];
    else
    {
        lastletter = stack[temp--];
        //output = lastletter + revstring(stack);
        //return output;
        return (lastletter + revstring(stack));
        //return ' '; //??????
    }
    printf("\n");
}
int main()
{
// going to have input be a word and turn it into a stack of letters
    scanf("%c", &letter);
    while(letter != '\n')
    {
        stack[top+1] = letter;
        top++;
        scanf("%c", &letter);
    }

    displayword(top);
    //printf("top: %d\n", top);

    printf("now reverse...\n");
    printf("%c", revstring());

    return 0;
}
