#include <stdio.h>
#include <stdlib.h>

struct stack
{
    int data;
    struct stack *next;
};

struct stack *top = NULL;
struct stack *ptr;

int *display(struct stack *top)
{
    ptr = top;
    if(top == NULL) 
        printf("\nempty");
    else
    {
        while(ptr != NULL)
        {
            printf("%d\n", ptr -> data);
            ptr = ptr -> next;
        }
    }
    return 0;
}

int main()
{
    int num;
    // push until -1
    scanf("%d", &num);
    while(num != -1)
    {
        ptr = (struct stack*)malloc(sizeof(struct stack));//new node
        ptr -> data = num;//get data value
        if(top==NULL)//if empty
        {
            ptr -> next = NULL; //points to NULL
            top = ptr; // top is this one
        }
        else // not empty
        {
            ptr -> next = top; // new node point to top node
            top = ptr; // top is this one
        }
        scanf("%d", &num);
    } 

    display(top);

    int to_pop, i;
    scanf("%d", &to_pop);
    printf("...popping %d off the stack...\n", to_pop);
    for(i=0; i<to_pop; i++){
        ptr = top;
        if(top == NULL)
            printf("underflow\n");
        else
        {
            top = top -> next;
            printf("here you go: %d\n", ptr -> data);
            free(ptr);
        }
    }

    display(top);

    return 0;
}

