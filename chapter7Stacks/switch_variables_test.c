#include <stdio.h>

int a = 5;
int b = 1;

void test1()//nope
{
    a = b, b = a;
    printf("a=%d\nb=%d\n",a,b);
}
void test2()//python
{
    a,b = b,a;
}
void fine()
{
    int temp;
    temp = a;
    a = b, b = temp;
    printf("a=%d\nb=%d\n",a,b);
}

int main()
{
    fine();
    return 0;
}
