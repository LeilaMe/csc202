#include <stdio.h>
#define MAX 20

int stack[MAX], topA=-1, topB=MAX, rstack[MAX], i, val;

//stack A will be 3 8 10 4 29 3
//stack B will be 0 19 1 38 1 2 7 40 1 3 9 8 1

// I need to display stack a and b twice so I will define them up here
void displayA(int st[], int top)
{
    //display stack A
    if(top==-1)//empty
        printf("Stack A is empty\n");
    else
    {
        printf("Stack A\n");
        for(i=top;i>0;i--) // start at the top
            printf("%d\n", st[i]);
    }
}
void displayB(int st[], int top)
{
    //display stack B
    if(top==MAX)//empty
        printf("Stack B is empty\n");
    else
    {
        printf("Stack B\n");
        for(i=top;i<MAX;i++) // start at the top
            printf("%d\n", st[i]);
    }
}

int popA(){
    int output;
    if(topA == -1) //empty
    {
        printf("UNDERFLOW\n");
        val = -999; //??
    }
    else
    {
        output = stack[topA];
        topA--;
    }
    return output;
}

int popB(){
    int output;
    if(topB == MAX) //empty
    {
        printf("UNDERFLOW\n");
        val = -999; //??
    }
    else
    {
        output = stack[topB];
        topB++;
    }
    return output;
}

int main()
{
    int num;
    // push onto stack A
    // when it ends with -1 stop
    scanf("%d",&num);
    while(num != -1){
        if(topA==topB-1) // if full
            printf("OVERFLOW\n");
        else
        {
            topA+=1;//to the right
            stack[topA] = num;
        }
        scanf("%d",&num);
    }
    // push onto stack B, ends with -1
    scanf("%d",&num);
    while(num != -1){
        if(topB-1==topA) //if full
            printf("OVERFLOW\n");
        else
        {
            topB-= 1; //to the left
            stack[topB] = num;
        }
        scanf("%d",&num);
    }
    displayA(stack, topA);
    displayB(stack, topB);

    //reverse
    int rtopA = topA, rtopB = topB;
    //printf("%d%d%d%d\n", topA, rtopA, topB, rtopB);
    //do A first
    for(i=0;i<rtopA;i++) 
    {
        //printf("i: %d\n", i);
        val = popA();
        //printf("popped off: %d\n", val);
        rstack[i] = val;
        //printf("the new stack thing: %d\n", rstack[i]);
    }
    // do B next
    for(i=rtopB;i<MAX;i++)
    {
        //printf("i: %d\n", i);
        val = popB();
        //printf("popped off: %d\n", val);
        rstack[i] = val;
        //printf("the new stack thing: %d\n", rstack[i]);
    }

    displayA(rstack, rtopA);
    displayB(rstack, rtopB);

    printf("why\n");
    //display all of rstack
    for(i=MAX;i>=0;i--)
        printf("%d\n", rstack[i]);

    return 0;
}
