#include <stdio.h>
#define MAX 20
int stack[MAX], topA = -1, topB = MAX;

//from the book page 228-229
void display_stackA()
{
    int i;
    if(topA == -1)
        printf("Stack A is Empty\n");
    else
    {
        printf("displaying stack A\n");
        for(i = topA; i >= 0; i--) // remember it is i >= 0 not i > 0
            printf("%d\t", stack[i]);
        printf("\n");
    }
}
void display_stackB()
{
    int i;
    if(topB == MAX)
        printf("Stack B is Empty\n");
    else
    {
        printf("displaying stack B\n");
        for(i = topB; i < MAX; i++)
            printf("%d\t", stack[i]);
        printf("\n");
    }
}

int main()
{
    int num;
    // push onto stack A
    // when it ends with -1 stop
    scanf("%d",&num);
    while(num != -1){
        if(topA==topB-1) // if full
            printf("OVERFLOW\n");
        else
        {
            topA+=1;//to the right
            stack[topA] = num;
        }
        scanf("%d",&num);
    }
    // push onto stack B, ends with -1
    scanf("%d",&num);
    while(num != -1){
        if(topB-1==topA) //if full
            printf("OVERFLOW\n");
        else
        {
            topB-= 1; //to the left
            stack[topB] = num;
        }
        scanf("%d",&num);
    }

    display_stackA();
    display_stackB();

    // reverse
    int i, rstack[MAX], temp = topA, tempB = topB;
    //pop stack A and push to new array
    for(i=0;i<=temp;i++)
        rstack[i] = stack[topA--];
    for(i=MAX;i>tempB;i--)
        rstack[i] = stack[topB++];
   
    display_stackA();
   //display rstack 
    for(i = temp; i >= 0; i--) 
        printf("%d\t", rstack[i]);
    printf("\n");

    display_stackB();
    for(i = tempB+1; i <= MAX; i++)
        printf("%d\t", rstack[i]);
    printf("\n");

    return 0;
}
