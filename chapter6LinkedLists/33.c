#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *next;
};
struct node *start = NULL;

int main()
{
    struct node *new_node, *ptr;
    int num;
    scanf("%d", &num);
    while(num!=-1)
    {
        new_node = (struct node*)malloc(sizeof(struct node));
        new_node -> data=num;
        if(start==NULL)
        {
            new_node -> next = NULL;
            start = new_node;
        } else {
            ptr = start;
            while(ptr->next!=NULL){
                ptr=ptr->next;
            }
            ptr->next = new_node;
            new_node->next=NULL;
        }
        scanf("%d", &num);
    }
// display
    ptr = start;
    while(ptr != NULL)
    {
        printf("%d\n", ptr -> data);
        ptr = ptr -> next;
    }

    struct node *preptr, *temp;
    ptr = start;
    preptr = ptr;
    if(ptr -> next != NULL)
        ptr = ptr -> next;
    while(ptr -> next != NULL) {
        temp = ptr -> next;
        ptr -> next = preptr;
        preptr = ptr;
        ptr = temp;
    }
    ptr -> next = preptr;
    preptr = start;
    preptr -> next = NULL;
    start = ptr;

// display
    ptr = start;
    while(ptr != NULL)
    {
        printf("%d\n", ptr -> data);
        ptr = ptr -> next;
    }

    return 0;
}
