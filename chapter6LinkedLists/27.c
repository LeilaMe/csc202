#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *next;
};

struct node *start = NULL;

int main() 
{
    struct node *new_node, *ptr;
    int num;
    scanf("%d", &num);
    while(num!=-1)
    {
        new_node = (struct node*)malloc(sizeof(struct node));
        new_node -> data = num;
        if(start == NULL)
        {
            new_node -> next = new_node;
            start = new_node;
        }
        else
        {
            ptr = start;
            while(ptr -> next != start) {
                ptr = ptr -> next;
            }
            ptr -> next = new_node;
            new_node -> next = start;
        }
        scanf("%d", &num);
    }
    ptr = start;
    int count = 0;
    while(ptr -> next != start)
    {
        printf("%d\n", ptr -> data);
        if(ptr->data != 0)
        {
            count += 1;
        }
        printf("count: %d\n", count);
        ptr = ptr -> next;
    }
    printf("%d\n", ptr -> data);
    if(ptr->data != 0)
    {
        count += 1;
    }
    printf("count: %d\n", count);
    return 0;
}
