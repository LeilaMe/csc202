#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node *next;
    struct node *prev;
};

int main()
{
    struct node *start = NULL;
    struct node *new_node, *ptr;
    int num;
    scanf("%d", &num);
    while(num!=-1){
        if(start == NULL){
            new_node = (struct node*)malloc(sizeof(struct node));
            new_node -> prev = NULL;
            new_node -> data = num;
            new_node -> next = NULL;
            start = new_node;
        }else{
            ptr = start;
            new_node = (struct node*)malloc(sizeof(struct node));
            new_node -> data = num;
            while(ptr->next!=NULL){
                ptr = ptr -> next;
            }
            ptr -> next = new_node;
            new_node -> prev = ptr;
            new_node -> next = NULL;
        }
        scanf("%d", &num);
    }

    //display
    ptr = start;
    while(ptr!=NULL){
        printf("%d\n", ptr -> data);
        ptr = ptr -> next;
    }

    int nodenum;
    scanf("%d", &nodenum);
    printf("and now move node %d to beginning\n", nodenum);

    //move from middle to top (beginning)
    struct node *temp;
    ptr = start;
    new_node = (struct node*)malloc(sizeof(struct node));
    int i;
    for(i=0;i<nodenum-2;i++){
        ptr = ptr -> next;
    }
    temp = ptr -> next;
    new_node -> data = temp -> data;
    ptr -> next = temp -> next;
    temp -> next -> prev = ptr;
    free(temp);
    start -> prev = new_node;
    new_node -> prev = NULL;
    new_node -> next = start;
    start = new_node;

    //display
    ptr = start;
    while(ptr!=NULL){
        printf("%d\n", ptr -> data);
        ptr = ptr -> next;
    }

    return 0;
}
