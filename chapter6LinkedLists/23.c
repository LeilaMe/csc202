#include <stdio.h>
#include <stdlib.h>

struct node
{
    float data;
    struct node *next;
};

int main()
{
    struct node *start = NULL;
    struct node *new_node, *ptr;
    float num;
    scanf("%f", &num);
    while(num!=-1)
    {
        new_node = (struct node*)malloc(sizeof(struct node));
        new_node -> data=num;
        if(start == NULL)
        {
            new_node -> next = NULL;
            start = new_node;
        }
        else{
            ptr = start;
            while(ptr -> next != NULL)
            {
                ptr = ptr -> next;
            }
            ptr -> next = new_node;
            new_node -> next = NULL;
        }
        scanf("%f", &num);
    }
    float sum, mean = 0;
    int count = 0;
    ptr = start;
    while(ptr -> next != NULL)
    {
        count += 1;
        sum += ptr -> data;
        ptr = ptr -> next;
    }
    count += 1;
    sum += ptr -> data;

    mean = sum / count;
    printf("sum: %f, count: %d, mean: %f\n", sum, count, mean);
    return 0;
}
