html_doc = """
<html class="no-js" lang="en-US" ><head><meta charset="utf-8" /><script type="text/javascript">(window.NREUM||(NREUM={})).init={ajax:{deny_list:["bam.nr-data.net"]}};(window.NREUM||(NREUM={})).loader_config={licenseKey:"afb86e4eb8",applicationID:"59460826"};window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var i=e[n]={exports:{}};t[n][0].call(i.exports,function(e){var i=t[n][1][e];return r(i||e)},i,i.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(t,e,n){function r(){}function i(t,e,n,r){return function(){return s.recordSupportability("API/"+e+"/called"),o(t+e,[u.now()].concat(c(arguments)),n?null:this,r),n?void 0:this}}var o=t("handle"),a=t(9),c=t(10),f=t("ee").get("tracer"),u=t("loader"),s=t(4),d=NREUM;"undefined"==typeof window.newrelic&&(newrelic=d);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],l="api-",v=l+"ixn-";a(p,function(t,e){d[e]=i(l,e,!0,"api")}),d.addPageAction=i(l,"addPageAction",!0),d.setCurrentRouteName=i(l,"routeName",!0),e.exports=newrelic,d.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(t,e){var n={},r=this,i="function"==typeof e;return o(v+"tracer",[u.now(),t,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return e.apply(this,arguments)}catch(t){throw f.emit("fn-err",[arguments,this,t],n),t}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){m[e]=i(v,e)}),newrelic.noticeError=function(t,e){"string"==typeof t&&(t=new Error(t)),s.recordSupportability("API/noticeError/called"),o("err",[t,u.now(),!1,e])}},{}],2:[function(t,e,n){function r(t){if(NREUM.init){for(var e=NREUM.init,n=t.split("."),r=0;r<n.length-1;r++)if(e=e[n[r]],"object"!=typeof e)return;return e=e[n[n.length-1]]}}e.exports={getConfiguration:r}},{}],3:[function(t,e,n){var r=!1;try{var i=Object.defineProperty({},"passive",{get:function(){r=!0}});window.addEventListener("testPassive",null,i),window.removeEventListener("testPassive",null,i)}catch(o){}e.exports=function(t){return r?{passive:!0,capture:!!t}:!!t}},{}],4:[function(t,e,n){function r(t,e){var n=[a,t,{name:t},e];return o("storeMetric",n,null,"api"),n}function i(t,e){var n=[c,t,{name:t},e];return o("storeEventMetrics",n,null,"api"),n}var o=t("handle"),a="sm",c="cm";e.exports={constants:{SUPPORTABILITY_METRIC:a,CUSTOM_METRIC:c},recordSupportability:r,recordCustom:i}},{}],5:[function(t,e,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=t(11);e.exports=r,e.exports.offset=a,e.exports.getLastTimestamp=i},{}],6:[function(t,e,n){function r(t,e){var n=t.getEntries();n.forEach(function(t){"first-paint"===t.name?l("timing",["fp",Math.floor(t.startTime)]):"first-contentful-paint"===t.name&&l("timing",["fcp",Math.floor(t.startTime)])})}function i(t,e){var n=t.getEntries();if(n.length>0){var r=n[n.length-1];if(u&&u<r.startTime)return;var i=[r],o=a({});o&&i.push(o),l("lcp",i)}}function o(t){t.getEntries().forEach(function(t){t.hadRecentInput||l("cls",[t])})}function a(t){var e=navigator.connection||navigator.mozConnection||navigator.webkitConnection;if(e)return e.type&&(t["net-type"]=e.type),e.effectiveType&&(t["net-etype"]=e.effectiveType),e.rtt&&(t["net-rtt"]=e.rtt),e.downlink&&(t["net-dlink"]=e.downlink),t}function c(t){if(t instanceof y&&!w){var e=Math.round(t.timeStamp),n={type:t.type};a(n),e<=v.now()?n.fid=v.now()-e:e>v.offset&&e<=Date.now()?(e-=v.offset,n.fid=v.now()-e):e=v.now(),w=!0,l("timing",["fi",e,n])}}function f(t){"hidden"===t&&(u=v.now(),l("pageHide",[u]))}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var u,s,d,p,l=t("handle"),v=t("loader"),m=t(8),g=t(3),y=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){s=new PerformanceObserver(r);try{s.observe({entryTypes:["paint"]})}catch(h){}d=new PerformanceObserver(i);try{d.observe({entryTypes:["largest-contentful-paint"]})}catch(h){}p=new PerformanceObserver(o);try{p.observe({type:"layout-shift",buffered:!0})}catch(h){}}if("addEventListener"in document){var w=!1,b=["click","keydown","mousedown","pointerdown","touchstart"];b.forEach(function(t){document.addEventListener(t,c,g(!1))})}m(f)}},{}],7:[function(t,e,n){function r(t,e){if(!i)return!1;if(t!==i)return!1;if(!e)return!0;if(!o)return!1;for(var n=o.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}e.exports={agent:i,version:o,match:r}},{}],8:[function(t,e,n){function r(t){function e(){t(c&&document[c]?document[c]:document[o]?"hidden":"visible")}"addEventListener"in document&&a&&document.addEventListener(a,e,i(!1))}var i=t(3);e.exports=r;var o,a,c;"undefined"!=typeof document.hidden?(o="hidden",a="visibilitychange",c="visibilityState"):"undefined"!=typeof document.msHidden?(o="msHidden",a="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(o="webkitHidden",a="webkitvisibilitychange",c="webkitVisibilityState")},{}],9:[function(t,e,n){function r(t,e){var n=[],r="",o=0;for(r in t)i.call(t,r)&&(n[o]=e(r,t[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],10:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,i=n-e||0,o=Array(i<0?0:i);++r<i;)o[r]=t[e+r];return o}e.exports=r},{}],11:[function(t,e,n){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(t,e,n){function r(){}function i(t){function e(t){return t&&t instanceof r?t:t?u(t,f,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){t&&a&&t(n,r,i);for(var c=e(i),f=m(n),u=f.length,s=0;s<u;s++)f[s].apply(c,r);var p=d[w[n]];return p&&p.push([b,n,r,c]),c}}function o(t,e){h[t]=m(t).concat(e)}function v(t,e){var n=h[t];if(n)for(var r=0;r<n.length;r++)n[r]===e&&n.splice(r,1)}function m(t){return h[t]||[]}function g(t){return p[t]=p[t]||i(n)}function y(t,e){l.aborted||s(t,function(t,n){e=e||"feature",w[n]=e,e in d||(d[e]=[])})}var h={},w={},b={on:o,addEventListener:o,removeEventListener:v,emit:n,get:g,listeners:m,context:e,buffer:y,abort:c,aborted:!1};return b}function o(t){return u(t,f,a)}function a(){return new r}function c(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var f="nr@context",u=t("gos"),s=t(9),d={},p={},l=e.exports=i();e.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(t,e,n){function r(t,e,n){if(i.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return t[e]=r,r}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){i.buffer([t],r),i.emit(t,e,n)}var i=t("ee").get("handle");e.exports=r,r.ee=i},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,o,function(){return i++})}var i=1,o="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!M++){var t=T.info=NREUM.info,e=m.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&e))return u.abort();f(x,function(e,n){t[e]||(t[e]=n)});var n=a();c("mark",["onload",n+T.offset],null,"api"),c("timing",["load",n]);var r=m.createElement("script");0===t.agent.indexOf("http://")||0===t.agent.indexOf("https://")?r.src=t.agent:r.src=l+"://"+t.agent,e.parentNode.insertBefore(r,e)}}function i(){"complete"===m.readyState&&o()}function o(){c("mark",["domContent",a()+T.offset],null,"api")}var a=t(5),c=t("handle"),f=t(9),u=t("ee"),s=t(7),d=t(2),p=t(3),l=d.getConfiguration("ssl")===!1?"http":"https",v=window,m=v.document,g="addEventListener",y="attachEvent",h=v.XMLHttpRequest,w=h&&h.prototype,b=!1;NREUM.o={ST:setTimeout,SI:v.setImmediate,CT:clearTimeout,XHR:h,REQ:v.Request,EV:v.Event,PR:v.Promise,MO:v.MutationObserver};var E=""+location,x={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1216.min.js"},O=h&&w&&w[g]&&!/CriOS/.test(navigator.userAgent),T=e.exports={offset:a.getLastTimestamp(),now:a,origin:E,features:{},xhrWrappable:O,userAgent:s,disabled:b};if(!b){t(1),t(6),m[g]?(m[g]("DOMContentLoaded",o,p(!1)),v[g]("load",r,p(!1))):(m[y]("onreadystatechange",i),v[y]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var M=0}},{}],"wrap-function":[function(t,e,n){function r(t,e){function n(e,n,r,f,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,f],s],t)}c(n+"start",[o,a,f],s,u);try{return p=e.apply(a,o)}catch(v){throw c(n+"err",[o,a,v],s,u),v}finally{c(n+"end",[o,a,p],s,u)}}return a(e)?e:(n||(n=""),nrWrapper[p]=e,o(e,nrWrapper,t),nrWrapper)}function r(t,e,r,i,o){r||(r="");var c,f,u,s="-"===r.charAt(0);for(u=0;u<e.length;u++)f=e[u],c=t[f],a(c)||(t[f]=n(c,s?f+r:r,i,f,o))}function c(n,r,o,a){if(!v||e){var c=v;v=!0;try{t.emit(n,r,o,e,a)}catch(f){i([f,n,r,o],t)}v=c}}return t||(t=s),n.inPlace=r,n.flag=p,n}function i(t,e){e||(e=s);try{e.emit("internal-error",t)}catch(n){}}function o(t,e,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(t);return r.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(o){i([o],n)}for(var a in t)l.call(t,a)&&(e[a]=t[a]);return e}function a(t){return!(t&&t instanceof Function&&t.apply&&!t[p])}function c(t,e){var n=e(t);return n[p]=t,o(t,n,s),n}function f(t,e,n){var r=t[e];t[e]=c(r,n)}function u(){for(var t=arguments.length,e=new Array(t),n=0;n<t;++n)e[n]=arguments[n];return e}var s=t("ee"),d=t(10),p="nr@original",l=Object.prototype.hasOwnProperty,v=!1;e.exports=r,e.exports.wrapFunction=c,e.exports.wrapInPlace=f,e.exports.argsToArray=u},{}]},{},["loader"]);</script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

					<link rel="icon" href="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" sizes="32x32" />
<link rel="icon" href="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" sizes="192x192" />
<link rel="apple-touch-icon" href="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" />
<meta name="msapplication-TileImage" content="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" />
				<meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />

	<!-- This site is optimized with the Yoast SEO plugin v18.4.1 - https://yoast.com/wordpress/plugins/seo/ -->
	<title>Staff Directory - Thomas Jefferson</title>
	<link rel="canonical" href="https://jefferson.apsva.us/staff-directory/" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Staff Directory - Thomas Jefferson" />
	<meta property="og:url" content="https://jefferson.apsva.us/staff-directory/" />
	<meta property="og:site_name" content="Thomas Jefferson" />
	<meta property="article:modified_time" content="2016-06-24T21:33:32+00:00" />
	<meta name="twitter:card" content="summary" />
	<script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://jefferson.apsva.us/#website","url":"https://jefferson.apsva.us/","name":"Thomas Jefferson","description":"An IB World School","potentialAction":[{"@type":"SearchAction","target":{"@type":"EntryPoint","urlTemplate":"https://jefferson.apsva.us/?s={search_term_string}"},"query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"WebPage","@id":"https://jefferson.apsva.us/staff-directory/#webpage","url":"https://jefferson.apsva.us/staff-directory/","name":"Staff Directory - Thomas Jefferson","isPartOf":{"@id":"https://jefferson.apsva.us/#website"},"datePublished":"2016-03-05T19:40:21+00:00","dateModified":"2016-06-24T21:33:32+00:00","breadcrumb":{"@id":"https://jefferson.apsva.us/staff-directory/#breadcrumb"},"inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://jefferson.apsva.us/staff-directory/"]}]},{"@type":"BreadcrumbList","@id":"https://jefferson.apsva.us/staff-directory/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"name":"Home","item":"https://jefferson.apsva.us/"},{"@type":"ListItem","position":2,"name":"Staff Directory"}]}]}</script>
	<!-- / Yoast SEO plugin. -->


<link rel='dns-prefetch' href='//ajax.googleapis.com' />
<link rel='dns-prefetch' href='//jefferson.apsva.us' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel='stylesheet' id='gtranslate-style-css'  href='https://jefferson.apsva.us/wp-content/plugins/gtranslate/gtranslate-style24.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://jefferson.apsva.us/wp-includes/css/dist/block-library/style.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='google-open-sans-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C400%2C700%2C600' type='text/css' media='all' />
<link rel='stylesheet' id='main-stylesheet-css'  href='https://jefferson.apsva.us/wp-content/themes/apsmain/css/foundation.css?v=1652030682' type='text/css' media='all' />
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js' id='jquery-js'></script>
<script type='text/javascript' src='https://jefferson.apsva.us/wp-content/themes/apsmain/js/vendor/modernizr.js' id='modernizr-js'></script>
<script type='text/javascript' src='https://jefferson.apsva.us/wp-content/themes/apsmain/bower_components/what-input/dist/what-input.min.js' id='what-input-js'></script>
<script type='text/javascript' src='https://jefferson.apsva.us/wp-content/themes/apsmain/js/vendor/fastclick.js' id='fastclick-js'></script>
<link rel="https://api.w.org/" href="https://jefferson.apsva.us/wp-json/" /><link rel="alternate" type="application/json" href="https://jefferson.apsva.us/wp-json/wp/v2/pages/4" /><script>var gt_request_uri = '/staff-directory/';</script>
        <script type="text/javascript">
            var jQueryMigrateHelperHasSentDowngrade = false;

			window.onerror = function( msg, url, line, col, error ) {
				// Break out early, do not processing if a downgrade reqeust was already sent.
				if ( jQueryMigrateHelperHasSentDowngrade ) {
					return true;
                }

				var xhr = new XMLHttpRequest();
				var nonce = '26e35fe7dc';
				var jQueryFunctions = [
					'andSelf',
					'browser',
					'live',
					'boxModel',
					'support.boxModel',
					'size',
					'swap',
					'clean',
					'sub',
                ];
				var match_pattern = /\)\.(.+?) is not a function/;
                var erroredFunction = msg.match( match_pattern );

                // If there was no matching functions, do not try to downgrade.
                if ( typeof erroredFunction !== 'object' || typeof erroredFunction[1] === "undefined" || -1 === jQueryFunctions.indexOf( erroredFunction[1] ) ) {
                    return true;
                }

                // Set that we've now attempted a downgrade request.
                jQueryMigrateHelperHasSentDowngrade = true;

				xhr.open( 'POST', 'https://jefferson.apsva.us/wp-admin/admin-ajax.php' );
				xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
				xhr.onload = function () {
					var response,
                        reload = false;

					if ( 200 === xhr.status ) {
                        try {
                        	response = JSON.parse( xhr.response );

                        	reload = response.data.reload;
                        } catch ( e ) {
                        	reload = false;
                        }
                    }

					// Automatically reload the page if a deprecation caused an automatic downgrade, ensure visitors get the best possible experience.
					if ( reload ) {
						location.reload();
                    }
				};

				xhr.send( encodeURI( 'action=jquery-migrate-downgrade-version&_wpnonce=' + nonce ) );

				// Suppress error alerts in older browsers
				return true;
			}
        </script>

		<style>

.page-template-home .sticky-post .title {border-top: 3px solid #f2b600;}

.page-template-home .sticky-post .title:after {border-color: transparent #f2b600 transparent transparent;}

.page-template-home .post-card-element .thumb, .page-template-home .news .post .thumb, .page-template-home .video-pane .post .thumb {border-bottom: 6px solid #264d73;}

.page-template-home .home-title-area { background: linear-gradient(to right, #3c78b4 , #3c78b4 50%, #336699 50%, #336699);}

.page-template-home section.quicklinks>h1, .page-template-home .news>h1, .page-template-home .video-pane>h1, .page-template-home .calendar>h1, .page-template-home .twitter>h1 {background:#264d73}

.page-template-home .home-title-area h1 a {color:#336699;}

.page-template-home .home-title-area .menu li {border-top: 1px solid rgba(255,255,255,.4);border-bottom: 1px solid rgba(0,0,0,.75);}

.page-template-home .hompage-element, .page-template-home section.quicklinks, .page-template-home .news, .page-template-home .video-pane, .page-template-home .calendar, .page-template-home .twitter {border-top: 1px solid #336699; border-left: 1px solid #336699;}

.page-template-home .hompage-element:last-child, .page-template-home section.quicklinks:last-child, .page-template-home .news:last-child, .page-template-home .video-pane:last-child, .page-template-home .calendar:last-child, .page-template-home .twitter:last-child {border-right: 1px solid #336699;}

.page-template-home .home-title-area .title-container {background: #264d73;}

.section-pages-sidebar h1 {color: #336699;}

.top-bar-section li.active:not(.has-form) a:not(.button) { background: #3c78b4!important; }

.top-bar-section .dropdown li:not(.has-form):not(.active):hover>a:not(.button) {background-color: #555;color:#FFFFFF;background:#336699 }

.button {background-color: #3c78b4;border-color: #f2b600;}

button:hover, button:focus, .button:hover, .button:focus {background-color: #336699;}

.audience-page .shortcode-aud-menu {background:#336699}

.collapsible h2 {background:#336699; color: #f2b600}

.section-pages-sidebar .menu>.menu-item.current_page_item>a { background-color: #336699; }

.section-pages-sidebar .menu .sub-menu > .menu-item.current_page_item > a { background-color: #336699; }

.top-bar .name h1 a { color: #336699 }

.sc_events_calendar .sc_calendar_submit { background: #264d73 ; color: #fff }

.single-event .event-calendar-buttons a { background: #264d73 ; color: #fff }

#sc_calendar th { background: #264d73 ; border-color: #264d73 ; color: #fff }

a {color: #336699 ;}


</style>

<style>


</style>

<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link rel="icon" href="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" sizes="32x32" />
<link rel="icon" href="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" sizes="192x192" />
<link rel="apple-touch-icon" href="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" />
<meta name="msapplication-TileImage" content="https://jefferson.apsva.us/wp-content/uploads/sites/22/legacy_assets/jefferson/1bef9b1445-yellowjacket.png" />
	</head>
	<body class="page-template page-template-templates page-template-page-staff-directory page-template-templatespage-staff-directory-php page page-id-4">
		<a class="screen-reader-text" href="#skip-nav" tabindex="0">Skip Navigation</a>
	<div class="off-canvas-wrap" data-offcanvas role="region">
	<div class="inner-wrap">

	<nav class="tab-bar">
		<section class="left-small" aria-label="toggle menu">
			<a class="left-off-canvas-toggle" href="#">
                            <img style="margin: 6px; padding: 2px;"
                            src="https://jefferson.apsva.us/wp-content/themes/apsmain/assets/img/svg/menu.svg"
			    alt="Mobile Menu Icon"/>
                        </a>
		</section>
		<section class="middle tab-bar-section" aria-label="school title">

			<h1 class="title">
				<a href="https://jefferson.apsva.us/">

													Thomas Jefferson					<span class="school-type">Middle School</span>
								</a>
			</h1>

		</section>
	</nav>

	<div id="alert_container"></div>
<script id="sitewide_alert_template" type="text/html">
    <div class="site-wide-alert-container alert-box <%= data.alert_style %>" data-alert="">
        <section class="row">
            <div class="columns small-16">
                <h1><%= data.alert_title %></h1>
                <h2><%= data.title %> <a href="<%= data.button_url %>"><%= data.button_text %></a>
                </h2>
                <a href="#" class="close"></a>
            </div>
        </section>
    </div>
</script>	<section id="schoolList">
	<div class="header-container">
		<div class="row">
			<h1 class="columns small-16 medium-10">Select a School</h1>
			<div class="columns small-16 medium-3 district-link">
				<a href="http://apsva.us/" class="button radius small">APS Home</a>
			</div>
		</div>
	</div>
	<div class="row school-list-items">
		<ul>
										<li><a href="#">ELEMENTARY SCHOOLS</a></li>
							<li><a href="https://abingdon.apsva.us">Abingdon</a></li>
							<li><a href="https://asfs.apsva.us">Arlington Science Focus</a></li>
							<li><a href="https://ats.apsva.us">Arlington Traditional</a></li>
							<li><a href="https://ashlawn.apsva.us">Ashlawn</a></li>
							<li><a href="https://barcroft.apsva.us">Barcroft</a></li>
							<li><a href="https://barrett.apsva.us">Barrett</a></li>
							<li><a href="https://campbell.apsva.us">Campbell</a></li>
							<li><a href="https://cardinal.apsva.us">Cardinal</a></li>
							<li><a href="https://carlinsprings.apsva.us">Carlin Springs</a></li>
							<li><a href="https://claremont.apsva.us">Claremont</a></li>
							<li><a href="https://discovery.apsva.us">Discovery</a></li>
							<li><a href="https://drew.apsva.us">Drew</a></li>
							<li><a href="https://fleet.apsva.us">Fleet</a></li>
							<li><a href="https://glebe.apsva.us">Glebe</a></li>
							<li><a href="https://hoffmanboston.apsva.us">Hoffman-Boston</a></li>
							<li><a href="https://innovation.apsva.us/">Innovation</a></li>
							<li><a href="https://www.apsva.us/special-education/integration-station/">Integration Station</a></li>
							<li><a href="https://jamestown.apsva.us">Jamestown</a></li>
							<li><a href="https://key.apsva.us">Key</a></li>
							<li><a href="https://longbranch.apsva.us">Long Branch</a></li>
							<li><a href="https://montessori.apsva.us/">Montessori</a></li>
							<li><a href="https://nottingham.apsva.us">Nottingham</a></li>
							<li><a href="https://oakridge.apsva.us">Oakridge</a></li>
							<li><a href="https://randolph.apsva.us">Randolph</a></li>
							<li><a href="https://taylor.apsva.us">Taylor</a></li>
							<li><a href="https://tuckahoe.apsva.us">Tuckahoe</a></li>
							<li><a href="#">MIDDLE SCHOOLS</a></li>
							<li><a href="https://dorothyhamm.apsva.us">Dorothy Hamm</a></li>
							<li><a href="https://gunston.apsva.us">Gunston</a></li>
							<li><a href="https://jefferson.apsva.us">Jefferson</a></li>
							<li><a href="https://kenmore.apsva.us">Kenmore</a></li>
							<li><a href="https://swanson.apsva.us">Swanson</a></li>
							<li><a href="https://williamsburg.apsva.us">Williamsburg</a></li>
							<li><a href="#">HIGH SCHOOLS & PROGRAMS</a></li>
							<li><a href="https://careercenter.apsva.us">Arlington Career Center</a></li>
							<li><a href="https://careercenter.apsva.us/arlington-tech/">Arlington Tech</a></li>
							<li><a href="https://achs.apsva.us">Arlington Community High School</a></li>
							<li><a href="https://hbwoodlawn.apsva.us">H-B Woodlawn</a></li>
							<li><a href="https://wakefield.apsva.us">Wakefield</a></li>
							<li><a href="https://wl.apsva.us">Washington-Liberty</a></li>
							<li><a href="https://yhs.apsva.us">Yorktown</a></li>
							<li><a href="https://hsc.apsva.us">Langston</a></li>
							<li><a href="https://newdirections.apsva.us">New Directions</a></li>
							<li><a href="https://shriver.apsva.us">Shriver</a></li>
							<li><a href="https://vlp.apsva.us">Virtual Learning Program (VLP)</a></li>
							<li><a href="https://apsva.us">DISTRICT SITE</a></li>
					</ul>
	</div>
</section>
<style type="text/css">
    #goog-gt-tt {display:none !important;}
    .goog-te-banner-frame {display:none !important;}
    .goog-te-menu-value:hover {text-decoration:none !important;}
    .goog-text-highlight {background-color:transparent !important;box-shadow:none !important;}
    body {top:0 !important;}
    #google_translate_element2 {display:none!important;}
</style>

<section id="translationList">
    <div class="row">
        <div class="columns small-16">
            <ul class="small-block-grid-1 medium-block-grid-3 translate-list-items">
                    <div><a href="https://www.apsva.us/wp-content/uploads/disclaimer.pdf">Disclaimer</a></div>
                            </ul>
        </div>
    </div>
</section>
	<aside class="left-off-canvas-menu">
    <ul class="header-icons">
                    <li>
                <a class="emergency-alerts" href="https://www.apsva.us/emergency-alerts/"><span
                        class="aps-icon-emergency"></span>Emergency Alerts</a>
            </li>
                            <li>
                <a class="login" href="https://jefferson.apsva.us/wp-login.php"><span class="aps-icon-login"></span>Login</a>
            </li>
                <li>
            <a class="blackboard" href="https://apsva.instructure.com/"><span
                    class="aps-icon-canvas"></span>Canvas</a>
        </li>
                <li>
            <a class="contact-us"
               href="https://jefferson.apsva.us/contact-main-office-staff/"><span
                    class="aps-icon-email"></span>Contact Us</a>
        </li>
    </ul>
     <!-- close the off-canvas menu -->
    <ul id="menu-main-menu" class="off-canvas-list"><li id="menu-item-666" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-666"><a href="#">About</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-4849" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4849"><a href="https://jefferson.apsva.us/about/mission-vision-and-values/">Mission, Vision, and Values</a></li>
	<li id="menu-item-788" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-788"><a href="https://jefferson.apsva.us/wp-content/uploads/sites/22/2021/09/Communication-Plan-2021-2022.pdf">Communications Plan</a></li>
	<li id="menu-item-789" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-789"><a href="http://jefferson.apsva.us/wp-content/uploads/legacy_assets/jefferson/4457576840-Thomas_Jefferson_Report_on_Family_Engagement__Updated.pdf">Family Engagement</a></li>
	<li id="menu-item-1199" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1199"><a href="https://jefferson.apsva.us/about/meet-the-staff/administration-counseling-and-support-staff/">Meet the Staff</a></li>
	<li id="menu-item-806" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-806"><a href="https://www.apsva.us/school-locations/school-management-plans/">School Management Plan</a></li>
	<li id="menu-item-740" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-740"><a href="https://www.apsva.us/school-overviews/jefferson-middle-school/">School Profile</a></li>
	<li id="menu-item-741" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-741"><a target="_blank" rel="noopener" href="http://schoolquality.virginia.gov/schools/jefferson-middle">VDOE Report Card</a></li>
	<li id="menu-item-4409" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4409"><a href="https://jefferson.apsva.us/about/volunteer-opportunities/">Volunteer Opportunities</a></li>
	<li id="menu-item-7636" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7636"><a href="https://jefferson.apsva.us/school-supply-list/">School Supply List</a></li>
</ul>
</li>
<li id="menu-item-2006" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-2006"><a href="https://jefferson.apsva.us/contact-main-office-staff/">Contact Us</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>
                            <li>
                    <a href="https://jefferson.apsva.us/contact-main-office-staff/">
                        Contact Us                    </a>
                </li>

            	<li id="menu-item-7723" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7723"><a href="https://jefferson.apsva.us/contact-main-office-staff/">Contact Main Office</a></li>
	<li id="menu-item-7722" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7722"><a href="https://jefferson.apsva.us/contact-academic-team-leaders/">Contact Academic Team Leaders</a></li>
	<li id="menu-item-9085" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9085"><a href="https://jefferson.apsva.us/about/contact-us/who-to-contact-if/">Who to contact if . . .</a></li>
</ul>
</li>
<li id="menu-item-667" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-667"><a href="#">Academics</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-9045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9045"><a href="https://jefferson.apsva.us/jefferson-gifted-services/">Jefferson Gifted Services</a></li>
	<li id="menu-item-719" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-719"><a href="https://jefferson.apsva.us/academic-programs/english-language-arts/">Language and Literature</a></li>
	<li id="menu-item-5862" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5862"><a href="https://jefferson.apsva.us/math-inventory/">Mathematics</a></li>
	<li id="menu-item-7701" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7701"><a href="https://jefferson.apsva.us/sciences/">Sciences</a></li>
	<li id="menu-item-6906" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6906"><a href="https://jefferson.apsva.us/academic-programs/world-languages/">Language Acquisition (World Languages)</a></li>
	<li id="menu-item-7715" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7715"><a href="https://jefferson.apsva.us/individuals-and-societies/">Individuals and Societies</a></li>
	<li id="menu-item-1858" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1858"><a href="https://jefferson.apsva.us/health-physical-education/">Health and Physical Education</a></li>
	<li id="menu-item-7703" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-7703"><a href="https://jefferson.apsva.us/arts/">Arts</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>
                            <li>
                    <a href="https://jefferson.apsva.us/arts/">
                        Arts                    </a>
                </li>

            		<li id="menu-item-7705" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7705"><a href="https://jefferson.apsva.us/academic-programs/artprogram/">Visual Arts</a></li>
		<li id="menu-item-7707" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7707"><a href="https://jefferson.apsva.us/drama/">Drama</a></li>
		<li id="menu-item-721" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-721"><a href="https://jefferson.apsva.us/academic-programs/band/">Band</a></li>
		<li id="menu-item-722" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-722"><a href="https://jefferson.apsva.us/academic-programs/chorus/">Chorus</a></li>
		<li id="menu-item-728" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-728"><a href="https://jefferson.apsva.us/academic-programs/orchestra/">Orchestra</a></li>
	</ul>
</li>
	<li id="menu-item-7710" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7710"><a href="https://jefferson.apsva.us/design/">Design</a></li>
	<li id="menu-item-7792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7792"><a href="https://jefferson.apsva.us/english-learners/">English Learners</a></li>
	<li id="menu-item-7708" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7708"><a href="https://jefferson.apsva.us/special-education-programs-3/">Special Education Programs</a></li>
	<li id="menu-item-2381" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2381"><a href="https://jefferson.apsva.us/counseling/testing/">Academic Testing</a></li>
</ul>
</li>
<li id="menu-item-668" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-668"><a href="#">Activities</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-9046" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9046"><a href="https://jefferson.apsva.us/activities/announcements/">Announcements</a></li>
	<li id="menu-item-9932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9932"><a href="https://jefferson.apsva.us/student-awards-and-highlights/">Student Awards and Highlights</a></li>
	<li id="menu-item-2192" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2192"><a href="http://jefferson.apsva.us/wp-content/uploads/sites/22/2016/11/After-School-Timeline.pdf">After School Timeline</a></li>
	<li id="menu-item-9681" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9681"><a href="https://jefferson.apsva.us/activities/arlington-prcr-after-school-teens-program-permission-slip/">Arlington PRCR After-School Teens Program/Permission Slip</a></li>
	<li id="menu-item-8671" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8671"><a href="https://jefferson.apsva.us/activities/sports-and-after-school-2021-2022/">Sports and After School 2021-2022</a></li>
	<li id="menu-item-734" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-734"><a href="https://jefferson.apsva.us/activities/physical-info/">Sports Physicals</a></li>
	<li id="menu-item-736" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-736"><a href="https://jefferson.apsva.us/activities/student-accident-insurance/">Student Accident Insurance</a></li>
	<li id="menu-item-730" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-730"><a href="https://jefferson.apsva.us/activities/arlington-soccer-association/">Arlington Soccer Association</a></li>
</ul>
</li>
<li id="menu-item-959" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-959"><a href="#">Counseling</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-947" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-947"><a href="https://jefferson.apsva.us/counseling/jefferson-school-counseling/">Welcome to Counseling</a></li>
	<li id="menu-item-948" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-948"><a href="https://jefferson.apsva.us/counseling/student-services-staff/">Student Services Staff</a></li>
	<li id="menu-item-1224" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1224"><a href="https://jefferson.apsva.us/counseling/aspire2excellence/">School Counseling Resources</a></li>
	<li id="menu-item-726" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-726"><a href="https://jefferson.apsva.us/counseling/access-naviance/">Access Naviance</a></li>
	<li id="menu-item-951" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-951"><a href="https://jefferson.apsva.us/counseling/calendar-of-school-counseling-programs/">Calendars of School Counseling Programs</a></li>
	<li id="menu-item-952" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-952"><a href="https://docs.google.com/forms/d/1b856sdI4BfX8UtPGfbCkdlarZMP4cy9-H5JoVXNmd4M/viewform?c=0&#038;w=1">Referral to Counseling</a></li>
	<li id="menu-item-954" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-954"><a href="http://apsva.us/mental-health-services/in-crisis-need-help-now/">In Crisis/Need Help Now?</a></li>
	<li id="menu-item-1386" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1386"><a href="https://jefferson.apsva.us/welcome-rising-6th-grade-families/">Welcome New Students and Families</a></li>
	<li id="menu-item-6119" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6119"><a href="https://jefferson.apsva.us/upcoming-events/">Upcoming Events</a></li>
</ul>
</li>
<li id="menu-item-670" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-670"><a href="#">IB</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-742" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-742"><a href="https://jefferson.apsva.us/ib-programme/">IB Programme</a></li>
	<li id="menu-item-8343" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8343"><a href="https://jefferson.apsva.us/ib-programme/ib-programme-newsletter/">IB Programme Newsletter</a></li>
	<li id="menu-item-8363" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8363"><a href="https://jefferson.apsva.us/ib-programme/the-ib-mission-and-learner-profile/">The IB Mission and Learner Profile</a></li>
	<li id="menu-item-5832" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5832"><a href="https://jefferson.apsva.us/ib/policies-program-access/">Written Curriculum</a></li>
	<li id="menu-item-5833" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5833"><a href="https://jefferson.apsva.us/ib-programme/wp-contentuploadssites22201703tjms-spring-service-opportunities-pdf/">Service Learning</a></li>
	<li id="menu-item-2409" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2409"><a href="https://jefferson.apsva.us/ib/community-project/">Community Project</a></li>
	<li id="menu-item-8351" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8351"><a href="https://jefferson.apsva.us/ib/community-project-guide-and-journal/">Community Project Lessons</a></li>
	<li id="menu-item-2018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018"><a href="https://jefferson.apsva.us/ib/academic-honesty-policy/">Academic Integrity Policy</a></li>
	<li id="menu-item-5834" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5834"><a href="https://jefferson.apsva.us/ib/policies-program-access/assessment-policy/">Assessment Policy</a></li>
	<li id="menu-item-5835" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5835"><a href="https://jefferson.apsva.us/ib/policies-program-access/language-policy/">Language Policy</a></li>
	<li id="menu-item-5836" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5836"><a href="https://jefferson.apsva.us/ib/policies-program-access/inclusion-policy/">Inclusion Policy</a></li>
	<li id="menu-item-5837" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5837"><a href="https://jefferson.apsva.us/ib-programme/ib-documents-for-parents/">Additional IB Resources &amp; FAQ</a></li>
</ul>
</li>
<li id="menu-item-7867" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-7867"><a href="#">Equity</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-7868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7868"><a href="https://jefferson.apsva.us/programs/equitycoordinator/">Diversity, Equity and Inclusion Coordinator</a></li>
	<li id="menu-item-7991" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7991"><a href="https://jefferson.apsva.us/contact-the-equity-coordinator/">Contact the Equity Coordinator</a></li>
	<li id="menu-item-7864" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7864"><a href="https://jefferson.apsva.us/canvas-for-parents-student-app-tutorials/">Canvas for Parents- Student App Tutorials</a></li>
	<li id="menu-item-7865" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7865"><a href="https://jefferson.apsva.us/gmu-eip-participants/">GMU EIP Participants</a></li>
	<li id="menu-item-7866" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7866"><a href="https://jefferson.apsva.us/hispanic-heritage-month-cosechando-suenos-para-nuestros-ninos/">Hispanic Heritage Month- Cosechando Sueños para Nuestros Niños</a></li>
</ul>
</li>
<li id="menu-item-671" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-671"><a href="#">Library</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-766" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-766"><a href="https://jefferson.apsva.us/library/thomas-jefferson-library/">Jefferson Library</a></li>
	<li id="menu-item-7614" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7614"><a href="https://jefferson.apsva.us/summer-reading/">Summer Reading!</a></li>
	<li id="menu-item-6507" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6507"><a href="https://search.follettsoftware.com/metasearch/ui/695">Library Online Catalog</a></li>
	<li id="menu-item-4470" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4470"><a href="https://jefferson.apsva.us/battle-books-2022/">2022 Battle of the Books</a></li>
	<li id="menu-item-764" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-764"><a href="https://jefferson.apsva.us/library/about-the-library/">About the Library</a></li>
	<li id="menu-item-6148" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6148"><a href="https://jefferson.apsva.us/library-newsletter/">Library Newsletter</a></li>
	<li id="menu-item-1006" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1006"><a href="https://jefferson.apsva.us/library/for-students/using-library-apps/">E-Books</a></li>
	<li id="menu-item-773" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-773"><a href="https://jefferson.apsva.us/library/welcome-to-tab/">TAB Club</a></li>
	<li id="menu-item-767" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-767"><a href="https://jefferson.apsva.us/library/research-databases/">Research Databases</a></li>
	<li id="menu-item-1005" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1005"><a href="https://jefferson.apsva.us/library/class-projects-research-linksclass-projects/">Class Projects &amp; Research Links</a></li>
	<li id="menu-item-771" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-771"><a href="https://jefferson.apsva.us/library/bibliography/">Bibliography (Reference List)</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>
                            <li>
                    <a href="https://jefferson.apsva.us/library/bibliography/">
                        Bibliography (Reference List)                    </a>
                </li>

            		<li id="menu-item-772" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-772"><a target="_blank" rel="noopener" href="http://my.noodletools.com/logon/signin?group=18714&#038;code=1290">NoodleTools</a></li>
	</ul>
</li>
	<li id="menu-item-778" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-778"><a href="https://jefferson.apsva.us/library/for-students/">Library Resources for Students</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>
                            <li>
                    <a href="https://jefferson.apsva.us/library/for-students/">
                        Library Resources for Students                    </a>
                </li>

            		<li id="menu-item-4295" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4295"><a href="https://jefferson.apsva.us/library/for-students/listen/">Listen to Books</a></li>
	</ul>
</li>
	<li id="menu-item-776" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-776"><a href="https://jefferson.apsva.us/library/library-resources-for-staff/">Library Resources for Staff</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>
                            <li>
                    <a href="https://jefferson.apsva.us/library/library-resources-for-staff/">
                        Library Resources for Staff                    </a>
                </li>

            		<li id="menu-item-765" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-765"><a target="_blank" rel="noopener" href="https://apsva.follettdestiny.com/common/welcome.jsp?site=118&#038;context=saas39_4504978">Library Catalog</a></li>
	</ul>
</li>
	<li id="menu-item-2043" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2043"><a href="https://jefferson.apsva.us/library/library-instructional-videos/">Library Instructional Videos</a></li>
</ul>
</li>
<li id="menu-item-1177" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1177"><a href="https://jefferson.apsva.us/pta/">TJMS PTA</a></li>
<li id="menu-item-1294" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1294"><a href="https://jefferson.apsva.us/staff/getting-started-quick-tips/">Staff</a></li>
<li id="menu-item-8851" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu menu-item-8851"><a href="#">Technology</a>            <ul class="left-submenu">
            <li class="back">
                <a href="#">
                    Back                </a>
            </li>

            	<li id="menu-item-786" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-786"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/">Personalized Learning Initiative</a></li>
	<li id="menu-item-7757" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7757"><a href="https://jefferson.apsva.us/middle-school-student-technology-tutorials/">Middle School Student Technology Tutorials</a></li>
	<li id="menu-item-8821" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8821"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/lost-or-damaged-ipad/">Service Requests for Student iPads</a></li>
	<li id="menu-item-8002" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8002"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/myaccess/">MyAccess</a></li>
	<li id="menu-item-6084" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6084"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/canvas-for-students/">Canvas for Students and Parents</a></li>
	<li id="menu-item-6085" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6085"><a href="https://jefferson.apsva.us/canvas-for-ta/">Canvas for TA</a></li>
</ul>
</li>
</ul>    <button class="screen-reader-text close-menu">Close Off-Canvas Menu</button>
</aside>


<script type="text/javascript">
    function doGTranslate(lang_pair) {if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];var plang=location.pathname.split('/')[1];if(plang.length !=2 && plang != 'zh-CN' && plang != 'zh-TW' && plang != 'hmn' && plang != 'haw' && plang != 'ceb')plang='en';if(lang == 'en')location.href=location.protocol+'//'+location.host+gt_request_uri;else location.href=location.protocol+'//'+location.host+'/'+lang+gt_request_uri;}
</script>

<div class="sitewide-header" id="sitewide-header">
	<div class="left">
		<a class="school-selector focus-skip-to" tabindex="0" >Our Schools</span><i class="fa fa-angle-double-up"></i></a>
		<!-- <a class="translate translate-selector" tabindex="0">Translate<span class="vertical-separator"></span><i class="fa fa-angle-double-up"></i></a> -->
        <a href="/es" onclick="doGTranslate('en|es');return false;" class="glink nturl notranslate main-language">Español</a>
        <a href="/mn" onclick="doGTranslate('en|mn');return false;" class="glink nturl notranslate main-language">Монгол</a>
        <a href="/am" onclick="doGTranslate('en|am');return false;" class="glink nturl notranslate main-language">አማርኛ</a>
        <a href="/ar" onclick="doGTranslate('en|ar');return false;" class="glink nturl notranslate main-language">العربية</a>
        <a style="display: inline-block;" class="translate-selector" role="button">
            <span class="screen-reader-text">Select a Language</span><i class="fa fa-globe"></i>
            <i class="fa fa-angle-down"></i>
        </a>
        <p style="display: inline-block; font-size: 0.6em;"><a href="/wp-content/uploads/disclaimer.pdf">Disclaimer</a></p>
	</div>

	<div class="right text-right">
					<a class="emergency-alerts" href="https://www.apsva.us/emergency-alerts/"><span class="aps-icon-emergency-solid"></span>Emergency Alerts</a>
							<a class="login" href="https://jefferson.apsva.us/wp-login.php"><span class="aps-icon-login-solid"></span>Login</a>
						<a class="contact-us" href="https://jefferson.apsva.us/contact-main-office-staff/"><span class="aps-icon-email-solid"></span>Contact Us</a>
		<a class="blackboard" href="https://apsva.instructure.com/"><span class="aps-icon-canvas-solid"></span>Canvas</a>
						<a class="contact-us" href="https://www.apsva.us"><span class="aps-icon-home-solid"></span>APS Site</a>
					<a class="search" href="#" id="header_search_tool"><span class="aps-icon-search-solid"></span><span class="search-label">Search</span></a>
	</div>
		<div role="search" id="topbarSearchForm" class="left topbar-search-form">
		<input type="text"
			   value=""
			   name="s" id="s"
			   placeholder="Search..."
			   title="Site Search"
			   class="topbar-search-input">
		<div class="selector-scope">
            <label for="search_scope_radio_specific">
                <input type="radio"
                   value="gcse-specific-search"
                   class="search_scope specific"
                   id="search_scope_radio_specific"
                   name="search_scope"
                       checked
                />
                This Site
            </label>
            <label for="search_scope_radio_general">
                <input type="radio"
                       id="search_scope_radio_general"
                       value="gcse-general-search"
                       class="search_scope general"
                       name="search_scope"
                    />
                All APS Sites
            </label>
        </div>
	</div>
	<i id="closeSearch" class="fa fa-times close-search"></i>
	</div>

<script src="https://www.google.com/jsapi"></script>
<script>
	(function() {
		var cx = '011557595227793964064:mr5j-waxeqq';
		var gcse = document.createElement('script');
		gcse.type = 'text/javascript';
		gcse.async = true;
		gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(gcse, s);
	})();
</script>
<div id="gcse-specific-search" style="width:0px;overflow:hidden;height:0px;"><!-- if you use display:none here, it doesn't work -->
	<gcse:search as_sitesearch='jefferson.apsva.us'></gcse:search>
</div>

<div id="gcse-general-search" style="width:0px;overflow:hidden;height:0px;"><!-- if you use display:none here, it doesn't work -->
	<gcse:search></gcse:search>
</div>

	<div class="top-bar-container contain-to-grid show-for-medium-up">
	<div class="skip-content"><a href="#skip-nav">Skip Navigation</a></div>
    <nav class="top-bar" data-topbar>
	    		    <ul class="title-area">
			    <li class="name">
				    					    <h1>
						    <a href='https://jefferson.apsva.us/' title='Thomas Jefferson' rel='home'>
								<img src='https://jefferson.apsva.us/wp-content/uploads/sites/22/2016/09/jefferson_logo_wide.jpg' alt='Thomas Jefferson'>
							</a>
					    </h1>

			    </li>
		    </ul>

        <section class="top-bar-section">
                        <ul id="menu-main-menu-1" class="top-bar-menu right"><li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-666"><a href="#">About</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4849"><a href="https://jefferson.apsva.us/about/mission-vision-and-values/">Mission, Vision, and Values</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-788"><a href="https://jefferson.apsva.us/wp-content/uploads/sites/22/2021/09/Communication-Plan-2021-2022.pdf">Communications Plan</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-789"><a href="http://jefferson.apsva.us/wp-content/uploads/legacy_assets/jefferson/4457576840-Thomas_Jefferson_Report_on_Family_Engagement__Updated.pdf">Family Engagement</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1199"><a href="https://jefferson.apsva.us/about/meet-the-staff/administration-counseling-and-support-staff/">Meet the Staff</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-806"><a href="https://www.apsva.us/school-locations/school-management-plans/">School Management Plan</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-740"><a href="https://www.apsva.us/school-overviews/jefferson-middle-school/">School Profile</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-741"><a target="_blank" rel="noopener" href="http://schoolquality.virginia.gov/schools/jefferson-middle">VDOE Report Card</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4409"><a href="https://jefferson.apsva.us/about/volunteer-opportunities/">Volunteer Opportunities</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7636"><a href="https://jefferson.apsva.us/school-supply-list/">School Supply List</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-2006"><a href="https://jefferson.apsva.us/contact-main-office-staff/">Contact Us</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7723"><a href="https://jefferson.apsva.us/contact-main-office-staff/">Contact Main Office</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7722"><a href="https://jefferson.apsva.us/contact-academic-team-leaders/">Contact Academic Team Leaders</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9085"><a href="https://jefferson.apsva.us/about/contact-us/who-to-contact-if/">Who to contact if . . .</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-667"><a href="#">Academics</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9045"><a href="https://jefferson.apsva.us/jefferson-gifted-services/">Jefferson Gifted Services</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-719"><a href="https://jefferson.apsva.us/academic-programs/english-language-arts/">Language and Literature</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5862"><a href="https://jefferson.apsva.us/math-inventory/">Mathematics</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7701"><a href="https://jefferson.apsva.us/sciences/">Sciences</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6906"><a href="https://jefferson.apsva.us/academic-programs/world-languages/">Language Acquisition (World Languages)</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7715"><a href="https://jefferson.apsva.us/individuals-and-societies/">Individuals and Societies</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1858"><a href="https://jefferson.apsva.us/health-physical-education/">Health and Physical Education</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-7703"><a href="https://jefferson.apsva.us/arts/">Arts</a>
<ul class="sub-menu dropdown">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7705"><a href="https://jefferson.apsva.us/academic-programs/artprogram/">Visual Arts</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7707"><a href="https://jefferson.apsva.us/drama/">Drama</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-721"><a href="https://jefferson.apsva.us/academic-programs/band/">Band</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-722"><a href="https://jefferson.apsva.us/academic-programs/chorus/">Chorus</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-728"><a href="https://jefferson.apsva.us/academic-programs/orchestra/">Orchestra</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7710"><a href="https://jefferson.apsva.us/design/">Design</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7792"><a href="https://jefferson.apsva.us/english-learners/">English Learners</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7708"><a href="https://jefferson.apsva.us/special-education-programs-3/">Special Education Programs</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2381"><a href="https://jefferson.apsva.us/counseling/testing/">Academic Testing</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-668"><a href="#">Activities</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9046"><a href="https://jefferson.apsva.us/activities/announcements/">Announcements</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9932"><a href="https://jefferson.apsva.us/student-awards-and-highlights/">Student Awards and Highlights</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2192"><a href="http://jefferson.apsva.us/wp-content/uploads/sites/22/2016/11/After-School-Timeline.pdf">After School Timeline</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9681"><a href="https://jefferson.apsva.us/activities/arlington-prcr-after-school-teens-program-permission-slip/">Arlington PRCR After-School Teens Program/Permission Slip</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8671"><a href="https://jefferson.apsva.us/activities/sports-and-after-school-2021-2022/">Sports and After School 2021-2022</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-734"><a href="https://jefferson.apsva.us/activities/physical-info/">Sports Physicals</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-736"><a href="https://jefferson.apsva.us/activities/student-accident-insurance/">Student Accident Insurance</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-730"><a href="https://jefferson.apsva.us/activities/arlington-soccer-association/">Arlington Soccer Association</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-959"><a href="#">Counseling</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-947"><a href="https://jefferson.apsva.us/counseling/jefferson-school-counseling/">Welcome to Counseling</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-948"><a href="https://jefferson.apsva.us/counseling/student-services-staff/">Student Services Staff</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1224"><a href="https://jefferson.apsva.us/counseling/aspire2excellence/">School Counseling Resources</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-726"><a href="https://jefferson.apsva.us/counseling/access-naviance/">Access Naviance</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-951"><a href="https://jefferson.apsva.us/counseling/calendar-of-school-counseling-programs/">Calendars of School Counseling Programs</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-952"><a href="https://docs.google.com/forms/d/1b856sdI4BfX8UtPGfbCkdlarZMP4cy9-H5JoVXNmd4M/viewform?c=0&#038;w=1">Referral to Counseling</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-954"><a href="http://apsva.us/mental-health-services/in-crisis-need-help-now/">In Crisis/Need Help Now?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1386"><a href="https://jefferson.apsva.us/welcome-rising-6th-grade-families/">Welcome New Students and Families</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6119"><a href="https://jefferson.apsva.us/upcoming-events/">Upcoming Events</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-670"><a href="#">IB</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-742"><a href="https://jefferson.apsva.us/ib-programme/">IB Programme</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8343"><a href="https://jefferson.apsva.us/ib-programme/ib-programme-newsletter/">IB Programme Newsletter</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8363"><a href="https://jefferson.apsva.us/ib-programme/the-ib-mission-and-learner-profile/">The IB Mission and Learner Profile</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5832"><a href="https://jefferson.apsva.us/ib/policies-program-access/">Written Curriculum</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5833"><a href="https://jefferson.apsva.us/ib-programme/wp-contentuploadssites22201703tjms-spring-service-opportunities-pdf/">Service Learning</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2409"><a href="https://jefferson.apsva.us/ib/community-project/">Community Project</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8351"><a href="https://jefferson.apsva.us/ib/community-project-guide-and-journal/">Community Project Lessons</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018"><a href="https://jefferson.apsva.us/ib/academic-honesty-policy/">Academic Integrity Policy</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5834"><a href="https://jefferson.apsva.us/ib/policies-program-access/assessment-policy/">Assessment Policy</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5835"><a href="https://jefferson.apsva.us/ib/policies-program-access/language-policy/">Language Policy</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5836"><a href="https://jefferson.apsva.us/ib/policies-program-access/inclusion-policy/">Inclusion Policy</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5837"><a href="https://jefferson.apsva.us/ib-programme/ib-documents-for-parents/">Additional IB Resources &amp; FAQ</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-7867"><a href="#">Equity</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7868"><a href="https://jefferson.apsva.us/programs/equitycoordinator/">Diversity, Equity and Inclusion Coordinator</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7991"><a href="https://jefferson.apsva.us/contact-the-equity-coordinator/">Contact the Equity Coordinator</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7864"><a href="https://jefferson.apsva.us/canvas-for-parents-student-app-tutorials/">Canvas for Parents- Student App Tutorials</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7865"><a href="https://jefferson.apsva.us/gmu-eip-participants/">GMU EIP Participants</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7866"><a href="https://jefferson.apsva.us/hispanic-heritage-month-cosechando-suenos-para-nuestros-ninos/">Hispanic Heritage Month- Cosechando Sueños para Nuestros Niños</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7989"><a href="https://jefferson.apsva.us/tjms-equity-team/">TJMS Equity Team</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-671"><a href="#">Library</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-766"><a href="https://jefferson.apsva.us/library/thomas-jefferson-library/">Jefferson Library</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7614"><a href="https://jefferson.apsva.us/summer-reading/">Summer Reading!</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6507"><a href="https://search.follettsoftware.com/metasearch/ui/695">Library Online Catalog</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4470"><a href="https://jefferson.apsva.us/battle-books-2022/">2022 Battle of the Books</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-764"><a href="https://jefferson.apsva.us/library/about-the-library/">About the Library</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6148"><a href="https://jefferson.apsva.us/library-newsletter/">Library Newsletter</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1006"><a href="https://jefferson.apsva.us/library/for-students/using-library-apps/">E-Books</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-773"><a href="https://jefferson.apsva.us/library/welcome-to-tab/">TAB Club</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-767"><a href="https://jefferson.apsva.us/library/research-databases/">Research Databases</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1005"><a href="https://jefferson.apsva.us/library/class-projects-research-linksclass-projects/">Class Projects &amp; Research Links</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-771"><a href="https://jefferson.apsva.us/library/bibliography/">Bibliography (Reference List)</a>
<ul class="sub-menu dropdown">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-772"><a target="_blank" rel="noopener" href="http://my.noodletools.com/logon/signin?group=18714&#038;code=1290">NoodleTools</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-778"><a href="https://jefferson.apsva.us/library/for-students/">Library Resources for Students</a>
<ul class="sub-menu dropdown">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4295"><a href="https://jefferson.apsva.us/library/for-students/listen/">Listen to Books</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-776"><a href="https://jefferson.apsva.us/library/library-resources-for-staff/">Library Resources for Staff</a>
<ul class="sub-menu dropdown">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-765"><a target="_blank" rel="noopener" href="https://apsva.follettdestiny.com/common/welcome.jsp?site=118&#038;context=saas39_4504978">Library Catalog</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2043"><a href="https://jefferson.apsva.us/library/library-instructional-videos/">Library Instructional Videos</a></li>
</ul>
</li>
<li class="divider"></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1177"><a href="https://jefferson.apsva.us/pta/">TJMS PTA</a></li>
<li class="divider"></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1294"><a href="https://jefferson.apsva.us/staff/getting-started-quick-tips/">Staff</a></li>
<li class="divider"></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-8851"><a href="#">Technology</a>
<ul class="sub-menu dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-786"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/">Personalized Learning Initiative</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7757"><a href="https://jefferson.apsva.us/middle-school-student-technology-tutorials/">Middle School Student Technology Tutorials</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8821"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/lost-or-damaged-ipad/">Service Requests for Student iPads</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8002"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/myaccess/">MyAccess</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6084"><a href="https://jefferson.apsva.us/technology-resources/digital-learning-initiative-updates/canvas-for-students/">Canvas for Students and Parents</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6085"><a href="https://jefferson.apsva.us/canvas-for-ta/">Canvas for TA</a></li>
</ul>
</li>
</ul>        </section>
    </nav>
</div>
<div id="skip-nav"></div>
<section class="container" role="document">
		<div class="row">
		<div class="staff-directory-content" role="main">

										<article class="post-4 page type-page status-publish hentry mat_pages_directory_group-families mat_pages_directory_group-staff mat_pages_directory_group-students" id="post-4">
					<header>
						<h1 class="entry-title">Staff Directory</h1>
					</header>
					<div class="entry-content">
											</div>
					        <div id="staff-directory-container" class="staff-directory-container">
            <div id="sd-controls" class="controls"></div>
            <section id="sd-content" class="content"></section>
        </div>

        <script>
                        aps_directory_data = {"E10363":{"name_f":"Martha","name_l":"Byron","email":"martha.byron@apsva.us","apsid":"E10363","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Math","Case Carrier","Science","Social Studies","Reading","8th Grade HR","English"],"house":["Eagles"],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTEwMzYz-----Martha-Byron"},"E12935":{"name_f":"Starr","name_l":"Lanman","email":"starr.lanman@apsva.us","apsid":"E12935","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Reading","Case Carrier"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTEyOTM1-----Starr-Lanman"},"E13073":{"name_f":"Matthew","name_l":"Hungerford","email":"matthew.hungerford@apsva.us","apsid":"E13073","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTEzMDcz-----Matthew-Hungerford"},"E1321":{"name_f":"Ann","name_l":"Miller","email":"ann.miller@apsva.us","apsid":"E1321","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["6th Grade HR","US History to Present"],"grade":["6"],"house":["Owls"],"category":"Instructional Staff","team":"Owls, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTEzMjE=-----Ann-Miller"},"E13656":{"name_f":"Diana","name_l":"Jordan","email":"diana.jordan@apsva.us","apsid":"E13656","classification":["Administrative Staff"],"site":["Jefferson MS"],"title":["Assistant Principal"],"category":"Administrative Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTEzNjU2-----Diana-Jordan"},"E13723":{"name_f":"Ana","name_l":"Mejia","email":"ana.mejia@apsva.us","apsid":"E13723","classification":["Staff"],"site":["Jefferson MS"],"title":["Administrative Assistant"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTEzNzIz-----Ana-Mejia"},"E15669":{"name_f":"Maureen","name_l":"Nolan","email":"maureen.nolan@apsva.us","apsid":"E15669","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["7th Grade HR","Civics and Economics"],"grade":["7"],"house":["Monarchs"],"category":"Instructional Staff","team":"Monarchs, 7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE1NjY5-----Maureen-Nolan"},"E15912":{"name_f":"Kristin","name_l":"Gomez","email":"kristin.gomez@apsva.us","apsid":"E15912","course":["Advanced Orchestra, Full Year ","Beginning Orchestra"],"grade":["6","7","8"],"site":["Jefferson MS"],"category":["Unclassified"],"team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE1OTEy-----Kristin-Gomez"},"E16418":{"name_f":"Kerry","name_l":"Fitzpatrick","email":"kerry.fitzpatrick@apsva.us","apsid":"E16418","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6"],"house":["Stingrays"],"course":["6th Grade HR","Grade 6 English "],"category":"Instructional Staff","team":"Stingrays, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE2NDE4-----Kerry-Fitzpatrick"},"E19269":{"name_f":"Aurelia","name_l":"Sicha","email":"aurelia.sicha@apsva.us","apsid":"E19269","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE5MjY5-----Aurelia-Sicha"},"E19288":{"name_f":"Elizabeth","name_l":"Egbert","email":"elizabeth.egbert@apsva.us","apsid":"E19288","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Introduction to Latin ","Latin I","Latin I, Intensified Full Year ","Latin II"],"grade":["6","7","8"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE5Mjg4-----Elizabeth-Egbert"},"E19472":{"name_f":"Robert","name_l":"Hanson","email":"robert.hanson@apsva.us","apsid":"E19472","classification":["Administrative Staff"],"site":["Jefferson MS"],"title":["Assistant Principal"],"category":"Administrative Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE5NDcy-----Robert-Hanson"},"E19579":{"name_f":"Heather","name_l":"Boda","email":"heather.boda@apsva.us","apsid":"E19579","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Teen Living","Life Management Skills, Semester","7th Grade HR","Family & Consumer Science"],"grade":["6","7","8"],"house":["Monarchs"],"category":"Instructional Staff","team":"Monarchs, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE5NTc5-----Heather-Boda"},"E19774":{"name_f":"Michelle","name_l":"Smith","email":"michelle.smith@apsva.us","apsid":"E19774","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTE5Nzc0-----Michelle-Smith"},"E20216":{"name_f":"Emily","name_l":"Calhoun","email":"emily.calhoun@apsva.us","apsid":"E20216","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Case Carrier","Instructional Studies","Social Skills","8th Grade HR"],"grade":["6","7","8"],"house":["Eagles"],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIwMjE2-----Emily-Calhoun"},"E20514":{"name_f":"Joanne","name_l":"Mann","email":"joanne.mann@apsva.us","apsid":"E20514","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"house":["Dolphins"],"grade":["6"],"course":["Grade 6 Reading"],"category":"Instructional Staff","team":"Dolphins, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIwNTE0-----Joanne-Mann"},"E20696":{"name_f":"Arnold","name_l":"Appanah","email":"arnold.appanah@apsva.us","apsid":"E20696","classification":["Staff"],"site":["Jefferson MS"],"title":["Manager"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIwNjk2-----Arnold-Appanah"},"E20732":{"name_f":"Patrick","name_l":"Whelden","email":"patrick.whelden@apsva.us","apsid":"E20732","classification":["Staff"],"site":["Jefferson MS"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIwNzMy-----Patrick-Whelden"},"E21063":{"name_f":"Enid","name_l":"Dunbar","email":"enid.dunbar@apsva.us","apsid":"E21063","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["8th Grade HR","ELD Case Manager"],"house":["Eagles"],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxMDYz-----Enid-Dunbar"},"E21066":{"name_f":"Velma","name_l":"Jones","email":"velma.jones@apsva.us","apsid":"E21066","classification":["Staff"],"site":["Jefferson MS"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxMDY2-----Velma-Jones"},"E21116":{"name_f":"Peter","name_l":"Anderson","email":"peter.anderson@apsva.us","apsid":"E21116","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["7th Grade HR","Grade 7 English "],"grade":["7"],"house":["Dragons"],"category":"Instructional Staff","team":"Dragons, 7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxMTE2-----Peter-Anderson"},"E21205":{"name_f":"Grace","name_l":"Waring","email":"grace.waring@apsva.us","apsid":"E21205","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Case Carrier","Reading","8th Grade HR"],"house":["Eagles"],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxMjA1-----Grace-Waring"},"E21329":{"name_f":"Amy","name_l":"Yosick","email":"amy.yosick@apsva.us","apsid":"E21329","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["6th Grade HR","Math Strategies 6","Math 6"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxMzI5-----Amy-Yosick"},"E21635":{"name_f":"Inger","name_l":"Moran","email":"inger.moran@apsva.us","apsid":"E21635","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"house":["Owls"],"course":["6th Grade HR","French II","French I, Intensified Full Year ","French I"],"category":"Instructional Staff","team":"Owls, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxNjM1-----Inger-Moran"},"E21648":{"name_f":"Irma","name_l":"De Leon-Veliz","email":"irma.deleonveliz@apsva.us","apsid":"E21648","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxNjQ4-----Irma-De Leon-Veliz"},"E21747":{"name_f":"Jaquelina","name_l":"Ferrara","email":"jaquelina.ferrara@apsva.us","apsid":"E21747","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Introduction to Spanish for Fluent Speakers","Spanish I","Spanish II","Introduction to Spanish ","Transitional Spanish ","Spanish I, Intensified"],"grade":["6","7","8"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxNzQ3-----Jaquelina-Ferrara"},"E21960":{"name_f":"Jennifer","name_l":"Vernier","email":"jennifer.vernier@apsva.us","apsid":"E21960","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Grade 7 Life Science ","7th Grade HR"],"grade":["7"],"house":["Monarchs"],"category":"Instructional Staff","team":"Monarchs, 7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIxOTYw-----Jennifer-Vernier"},"E22431":{"name_f":"Raven","name_l":"Armstrong","email":"raven.armstrong@apsva.us","apsid":"E22431","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIyNDMx-----Raven-Armstrong"},"E22975":{"name_f":"Katlyn","name_l":"Bennett","email":"katlyn.bennett@apsva.us","apsid":"E22975","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Grade 8 Reading Strategies, Full Year ","Grade 7 Reading Strategies "],"grade":["7","8"],"category":"Instructional Staff","team":"7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIyOTc1-----Katlyn-Bennett"},"E23179":{"name_f":"Mary","name_l":"Foley","email":"mary.foley@apsva.us","apsid":"E23179","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"house":["Eagles"],"course":["Grade 8 English "],"grade":["8"],"category":"Instructional Staff","team":"Eagles, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIzMTc5-----Mary-Foley"},"E23197":{"name_f":"Amelia","name_l":"Rosegrant","email":"amelia.rosegrant@apsva.us","apsid":"E23197","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIzMTk3-----Amelia-Rosegrant"},"E23392":{"name_f":"Sean","name_l":"Curran","email":"sean.curran@apsva.us","apsid":"E23392","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","8"],"course":["Grade 8 Health and Physical Education ","Grade 6 Health and Physical Education "],"category":"Instructional Staff","team":"6, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIzMzky-----Sean-Curran"},"E23755":{"name_f":"Brittany","name_l":"Wiltrout","email":"brittany.wiltrout@apsva.us","apsid":"E23755","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"house":["Eagles"],"course":["8th Grade HR","World Geography "],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIzNzU1-----Brittany-Wiltrout"},"E23795":{"name_f":"Keisha","name_l":"Boggan","email":"keisha.boggan@apsva.us","apsid":"E23795","site":["Jefferson MS"],"title":["Principal"],"category":["Unclassified"],"team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIzNzk1-----Keisha-Boggan"},"E23920":{"name_f":"Lakesha","name_l":"Gray","email":"lakesha.gray@apsva.us","apsid":"E23920","classification":["Staff"],"site":["Jefferson MS"],"title":["Administrative Assistant"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTIzOTIw-----Lakesha-Gray"},"E24016":{"name_f":"Sumer","name_l":"Majid","email":"sumer.majid@apsva.us","apsid":"E24016","grade":["7","8"],"course":["Arabic I, Intensified Full Year ","Arabic I","Arabic II"],"site":["Jefferson MS"],"category":["Unclassified"],"team":"7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0MDE2-----Sumer-Majid"},"E24153":{"name_f":"Kaila","name_l":"Leonberger","email":"kaila.leonberger@apsva.us","apsid":"E24153","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"house":["Gators"],"grade":["8"],"course":["8th Grade HR","Grade 8 Physical Science "],"category":"Instructional Staff","team":"Gators, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0MTUz-----Kaila-Leonberger"},"E24209":{"name_f":"Lisa","name_l":"Frayer","email":"lisa.frayer@apsva.us","apsid":"E24209","classification":["Staff"],"site":["Jefferson MS"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0MjA5-----Lisa-Frayer"},"E24486":{"name_f":"Stephanie","name_l":"Smith","email":"stephanie.smith@apsva.us","apsid":"E24486","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0NDg2-----Stephanie-Smith"},"E24561":{"name_f":"Caitlin","name_l":"Rotchford","email":"caitlin.rotchford@apsva.us","apsid":"E24561","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Geometry, Intensified ","Algebra I ","8th Grade HR"],"grade":["6","7","8"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0NTYx-----Caitlin-Rotchford"},"E24693":{"name_f":"Olivia","name_l":"Green","email":"olivia.green@apsva.us","apsid":"E24693","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0Njkz-----Olivia-Green"},"E24717":{"name_f":"Gary","name_l":"Hauptman","email":"gary.hauptman@apsva.us","apsid":"E24717","classification":["Staff"],"site":["Jefferson MS"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0NzE3-----Gary-Hauptman"},"E24796":{"name_f":"Rachel","name_l":"Payne","email":"rachel.payne@apsva.us","apsid":"E24796","classification":["Instructional Staff"],"title":["Teacher"],"site":["Jefferson MS"],"house":["Gators"],"grade":["8"],"course":["8th Grade HR","World Geography "],"category":"Instructional Staff","team":"Gators, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0Nzk2-----Rachel-Payne"},"E24911":{"name_f":"Christian","name_l":"Banach","email":"christian.banach@apsva.us","apsid":"E24911","site":["Jefferson MS"],"title":["Teacher"],"classification":["Instructional Staff"],"course":["Beginning Chorus ","Choral Music","Advanced Chorus","Intermediate Chorus"],"grade":["6","7","8"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0OTEx-----Christian-Banach"},"E24960":{"name_f":"Abigail","name_l":"Kaster","email":"abigail.kaster@apsva.us","apsid":"E24960","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Phys. Science","Case Carrier","8th Grade HR"],"house":["Gators"],"category":"Instructional Staff","team":"Gators, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI0OTYw-----Abigail-Kaster"},"E2550":{"name_f":"Elita","name_l":"Jenks","email":"elita.jenks@apsva.us","apsid":"E2550","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"house":["Gators"],"course":["8th Grade HR","ELD Case Manager"],"category":"Instructional Staff","team":"Gators, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1NTA=-----Elita-Jenks"},"E25564":{"name_f":"Lauren","name_l":"Negrete","email":"lauren.negrete@apsva.us","apsid":"E25564","classification":["Instructional Staff"],"site":["Jefferson MS"],"grade":["6","7","8"],"title":["Teacher"],"course":["ELD 4 Reading","7th Grade HR","ELD Case Manager"],"house":["Monarchs"],"category":"Instructional Staff","team":"Monarchs, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1NTY0-----Lauren-Negrete"},"E25576":{"name_f":"Vincent","name_l":"Jarosz","email":"vincent.jarosz@apsva.us","apsid":"E25576","site":["Jefferson MS"],"title":["Assistant Principal"],"classification":["Administrative Staff"],"category":"Administrative Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1NTc2-----Vincent-Jarosz"},"E25716":{"name_f":"Amelia","name_l":"Black","email":"amelia.black@apsva.us","apsid":"E25716","site":["Jefferson MS"],"title":["Teacher"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1NzE2-----Amelia-Black"},"E25823":{"name_f":"Tiffini","name_l":"Woody-Pope","email":"tiffini.woodypope@apsva.us","apsid":"E25823","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1ODIz-----Tiffini-Woody-Pope"},"E25842":{"name_f":"Kara","name_l":"McPhillips","email":"kara.mcphillips@apsva.us","apsid":"E25842","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"house":["Owls"],"course":["Grade 6 Reading"],"grade":["6"],"category":"Instructional Staff","team":"Owls, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1ODQy-----Kara-McPhillips"},"E25861":{"name_f":"Jessica","name_l":"Kramer","email":"jessica.kramer@apsva.us","apsid":"E25861","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Pre-Algebra for 8th Graders ","Algebra I ","8th Grade HR"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1ODYx-----Jessica-Kramer"},"E25889":{"name_f":"Dante","name_l":"Hicks","email":"dante.hicks@apsva.us","apsid":"E25889","classification":["Administrative Staff"],"title":["Director"],"site":["Jefferson MS"],"category":"Administrative Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI1ODg5-----Dante-Hicks"},"E26010":{"name_f":"Lei","name_l":"Shang","email":"lei.shang@apsva.us","apsid":"E26010","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["8th Grade HR","Pre-Algebra for 8th Graders","Case Carrier"],"grade":["6","7","8"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2MDEw-----Lei-Shang"},"E26086":{"name_f":"Dana","name_l":"Brundidge","email":"dana.brundidge@apsva.us","apsid":"E26086","classification":["Staff"],"title":["Administrative Assistant","Staff"],"site":["Jefferson MS"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2MDg2-----Dana-Brundidge"},"E26439":{"name_f":"Antonio","name_l":"Tondelli","email":"antonio.tondellipard@apsva.us","apsid":"E26439","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2NDM5-----Antonio-Tondelli"},"E26594":{"name_f":"Ana","name_l":"Rodriguez","email":"ana.rodriguez2@apsva.us","apsid":"E26594","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2NTk0-----Ana-Rodriguez"},"E26685":{"name_f":"Brandon","name_l":"Gladney","email":"brandon.gladney@apsva.us","apsid":"E26685","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"house":["Stingrays"],"course":["US History to Present","6th Grade HR"],"grade":["6"],"category":"Instructional Staff","team":"Stingrays, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2Njg1-----Brandon-Gladney"},"E26830":{"name_f":"Andrew","name_l":"Bridges","email":"andrew.bridges@apsva.us","apsid":"E26830","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"house":["Eagles"],"course":["8th Grade HR","Grade 8 Physical Science "],"grade":["8"],"category":"Instructional Staff","team":"Eagles, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2ODMw-----Andrew-Bridges"},"E26838":{"name_f":"Devon","name_l":"Riley","email":"devon.riley@apsva.us","apsid":"E26838","classification":["Instructional Staff"],"title":["Teacher"],"site":["Jefferson MS"],"house":["Stingrays"],"grade":["6","7","8"],"course":["Theater Arts","6th Grade HR","Drama","Theatre Arts-Drama"],"category":"Instructional Staff","team":"Stingrays, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2ODM4-----Devon-Riley"},"E26845":{"name_f":"Zachary","name_l":"Carter","email":"zachary.carter@apsva.us","apsid":"E26845","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Grade 7 Math ","Pre-Algebra for 7th Graders (7 and 8)","7th Grade HR"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2ODQ1-----Zachary-Carter"},"E26859":{"name_f":"Traci","name_l":"Holland","email":"traci.holland@apsva.us","apsid":"E26859","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Grade 7 Life Science ","7th Grade HR"],"house":["Dragons"],"grade":["7"],"category":"Instructional Staff","team":"Dragons, 7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2ODU5-----Traci-Holland"},"E26994":{"name_f":"Kelly","name_l":"Lopez","email":"kelly.lopez@apsva.us","apsid":"E26994","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Case Carrier","Reading"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI2OTk0-----Kelly-Lopez"},"E27439":{"name_f":"Miyatt","name_l":"Harrison","email":"miyatt.harrison@apsva.us","apsid":"E27439","site":["Jefferson MS"],"course":["6th Grade HR","Case Carrier","US History to Present"],"title":["Teacher"],"grade":["6","7","8"],"house":["Owls"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"Owls, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI3NDM5-----Miyatt-Harrison"},"E27838":{"name_f":"Iris","name_l":"Whitehill","email":"iris.whitehill@apsva.us","apsid":"E27838","grade":["7"],"course":["Spanish I, Intensified"],"site":["Jefferson MS"],"category":["Unclassified"],"team":"7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI3ODM4-----Iris-Whitehill"},"E27947":{"name_f":"Paul","name_l":"Perrot","email":"paul.perrot@apsva.us","apsid":"E27947","classification":["Instructional Staff"],"title":["Teacher"],"grade":["6"],"site":["Jefferson MS"],"course":["Introduction to French "],"category":"Instructional Staff","team":"6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI3OTQ3-----Paul-Perrot"},"E28076":{"name_f":"Anna","name_l":"LaVardera","email":"anna.lavardera@apsva.us","apsid":"E28076","classification":["Instructional Staff"],"site":["Jefferson MS"],"house":["Eagles"],"course":["Case Carrier","English 8","8th Grade HR"],"grade":["6","7","8"],"title":["Teacher"],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4MDc2-----Anna-LaVardera"},"E28123":{"name_f":"Camee","name_l":"Beyers","email":"camee.beyers@apsva.us","apsid":"E28123","site":["Jefferson MS"],"classification":["Instructional Staff"],"title":["Teacher"],"house":["Monarchs"],"course":["Grade 7 English "],"grade":["7"],"category":"Instructional Staff","team":"Monarchs, 7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4MTIz-----Camee-Beyers"},"E28251":{"name_f":"Claudia","name_l":"Martinez","email":"claudia.martinez@apsva.us","apsid":"E28251","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4MjUx-----Claudia-Martinez"},"E28583":{"name_f":"Nam","name_l":"Clark","email":"nam.clark2@apsva.us","apsid":"E28583","site":["Jefferson MS"],"classification":["Instructional Staff"],"house":["Stingrays"],"course":["ELD Case Manager","ELD 1 Reading","ELD 3 English","ELD 3 Reading","ELD 1 English","6th Grade HR"],"grade":["6","7","8"],"title":["Teacher"],"category":"Instructional Staff","team":"Stingrays, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4NTgz-----Nam-Clark"},"E28666":{"name_f":"Kiera","name_l":"Willis","email":"kiera.willis@apsva.us","apsid":"E28666","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4NjY2-----Kiera-Willis"},"E28951":{"name_f":"Susan","name_l":"Russo","email":"susan.russo@apsva.us","apsid":"E28951","site":["Jefferson MS"],"title":["Teacher"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4OTUx-----Susan-Russo"},"E28978":{"name_f":"Jill","name_l":"Miller","email":"jill.miller@apsva.us","apsid":"E28978","title":["Staff"],"classification":["Staff"],"site":["Jefferson MS"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4OTc4-----Jill-Miller"},"E28981":{"name_f":"Megan","name_l":"Detweiler","email":"megan.detweiler@apsva.us","apsid":"E28981","title":["Teacher"],"course":["7th Grade HR"],"classification":["Instructional Staff"],"grade":["7"],"house":["Monarchs"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Monarchs, 7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI4OTgx-----Megan-Detweiler"},"E29107":{"name_f":"Susan","name_l":"Wilkinson","email":"susan.wilkinson@apsva.us","apsid":"E29107","classification":["Instructional Staff"],"course":["Spanish II","Introduction to Spanish for Fluent Speakers","Spanish for Fluent Speakers I ","Spanish for Fluent Speakers II "],"grade":["6","7","8"],"title":["Teacher"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI5MTA3-----Susan-Wilkinson"},"E29129":{"name_f":"Kipchoge","name_l":"Dow","email":"kipchoge.dow@apsva.us","apsid":"E29129","classification":["Instructional Staff"],"site":["Jefferson MS"],"grade":["6","7","8"],"title":["Teacher"],"course":["Pre-Algebra for 7th Graders (7 and 8)","Grade 7 Math ","7th Grade HR"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI5MTI5-----Kipchoge-Dow"},"E29135":{"name_f":"Aisha","name_l":"Madhi","email":"aisha.madhi@apsva.us","apsid":"E29135","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["8"],"house":["Gators"],"course":["8th Grade HR","Grade 8 English "],"category":"Instructional Staff","team":"Gators, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI5MTM1-----Aisha-Madhi"},"E29208":{"name_f":"Chris","name_l":"Ammon","email":"chris.ammon@apsva.us","apsid":"E29208","site":["Jefferson MS"],"grade":["6","7","8"],"course":["Technology Education","Digital Input Technologies, Semester","8th Grade HR","Investigating Computer Science","Technology of Robotic Design "],"house":["Gators"],"classification":["Instructional Staff"],"title":["Teacher"],"category":"Instructional Staff","team":"Gators, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI5MjA4-----Chris-Ammon"},"E29805":{"name_f":"Enkhtuul","name_l":"Nyamaakhuu","email":"enkhtuul.nyamaakhuu@apsva.us","apsid":"E29805","classification":["Staff"],"title":["School Finance Officer"],"site":["Jefferson MS"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI5ODA1-----Enkhtuul-Nyamaakhuu"},"E29911":{"name_f":"Nigel","name_l":"Somerville","email":"nigel.somerville@apsva.us","apsid":"E29911","classification":["Instructional Staff"],"grade":["6","7","8"],"title":["Teacher"],"course":["7th Grade HR","Grade 7 Math ","Homeroom Middle School","Pre-Algebra for 7th Graders (7 and 8)"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTI5OTEx-----Nigel-Somerville"},"E30014":{"name_f":"Michael","name_l":"Sivells","email":"michael.sivells@apsva.us","apsid":"E30014","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["7th Grade HR","Civics and Economics"],"grade":["7"],"house":["Dragons"],"category":"Instructional Staff","team":"Dragons, 7","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMwMDE0-----Michael-Sivells"},"E30048":{"name_f":"Kip","name_l":"Malinosky","email":"kip.malinosky@apsva.us","apsid":"E30048","classification":["Instructional Staff"],"site":["Jefferson MS"],"house":["Dolphins"],"title":["Teacher"],"grade":["6"],"course":["6th Grade HR"],"category":"Instructional Staff","team":"Dolphins, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMwMDQ4-----Kip-Malinosky"},"E30109":{"name_f":"Belinda","name_l":"Snell","email":"belinda.snell@apsva.us","apsid":"E30109","classification":["Instructional Staff"],"house":["Gators"],"course":["8th Grade HR","Grade 8 Physical Science "],"grade":["8"],"title":["Teacher"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Gators, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMwMTA5-----Belinda-Snell"},"E30154":{"name_f":"Celestino","name_l":"Samaniego","email":"celestino.samaniego@apsva.us","apsid":"E30154","site":["Jefferson MS"],"title":["Teacher"],"classification":["Instructional Staff"],"grade":["6","7","8"],"course":["Math 6","Pre-Algebra for 6th Graders ","6th Grade HR"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMwMTU0-----Celestino-Samaniego"},"E30218":{"name_f":"Mathieu","name_l":"Bee","email":"mathieu.bee@apsva.us","apsid":"E30218","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"course":["Pre-Algebra for 6th Graders ","6th Grade HR","Math 6"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMwMjE4-----Mathieu-Bee"},"E3074":{"name_f":"Martin","name_l":"Heath","email":"martin.heath@apsva.us","apsid":"E3074","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"course":["Grade 7 Health and Physical Education ","Grade 8 Health and Physical Education ","6th Grade HR"],"grade":["6","7","8"],"house":["Stingrays"],"category":"Instructional Staff","team":"Stingrays, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMwNzQ=-----Martin-Heath"},"E31085":{"name_f":"Melissa","name_l":"Cobbs","email":"melissa.cobbs@apsva.us","apsid":"E31085","classification":["Instructional Staff"],"title":["Teacher"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxMDg1-----Melissa-Cobbs"},"E31220":{"name_f":"Shyrone","name_l":"Stith","email":"shyrone.stith@apsva.us","apsid":"E31220","classification":["Instructional Staff"],"site":["Jefferson MS"],"grade":["6","7","8"],"title":["Teacher"],"course":["Grade 7 Health and Physical Education ","Grade 8 Health and Physical Education "],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxMjIw-----Shyrone-Stith"},"E31263":{"name_f":"Jasmine","name_l":"Burton","email":"jasmine.burton@apsva.us","apsid":"E31263","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Teacher"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxMjYz-----Jasmine-Burton"},"E31265":{"name_f":"Stephanie","name_l":"Washington","email":"stephanie.washington@apsva.us","apsid":"E31265","classification":["Instructional Staff"],"site":["Jefferson MS"],"course":["ELD 3 English","ELD 3 Reading","ELD 1 English","ELD Case Manager","ELD 1 Reading","6th Grade HR"],"grade":["6","7","8"],"title":["Teacher"],"house":["Owls"],"category":"Instructional Staff","team":"Owls, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxMjY1-----Stephanie-Washington"},"E31267":{"name_f":"Jeffery","name_l":"Anderson","email":"jeffery.anderson@apsva.us","apsid":"E31267","course":["Computer Applications and Internet Technologies ","Digital Input Technologies, Semester","Business & IT","Yearbook, Full Year "],"title":["Teacher"],"grade":["6","7","8"],"site":["Jefferson MS"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxMjY3-----Jeffery-Anderson"},"E31295":{"name_f":"Britney","name_l":"Patterson","email":"britney.patterson@apsva.us","apsid":"E31295","course":["Grade 7 Health and Physical Education ","Grade 6 Health and Physical Education ","8th Grade HR"],"house":["Gators"],"title":["Teacher"],"classification":["Instructional Staff"],"grade":["6","7","8"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Gators, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxMjk1-----Britney-Patterson"},"E31406":{"name_f":"Jeremy","name_l":"Wintersteen","email":"jeremy.wintersteen@apsva.us","apsid":"E31406","site":["Jefferson MS"],"title":["Teacher"],"grade":["6","7","8"],"classification":["Instructional Staff"],"course":["Chinese I","Chinese II","Chinese I, Intensified Full Year ","Introduction to Chinese"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxNDA2-----Jeremy-Wintersteen"},"E31456":{"name_f":"Maysoon","name_l":"Salih","email":"maysoon.salih@apsva.us","apsid":"E31456","site":["Jefferson MS"],"course":["Introduction to Arabic"],"grade":["6"],"category":["Unclassified"],"team":"6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxNDU2-----Maysoon-Salih"},"E31527":{"name_f":"Wayne","name_l":"Snider","email":"wayne.snider@apsva.us","apsid":"E31527","classification":["Instructional Staff"],"grade":["6","7","8"],"title":["Teacher"],"course":["7th Grade HR","Instructional Studies","Case Carrier"],"site":["Jefferson MS"],"house":["Monarchs"],"category":"Instructional Staff","team":"Monarchs, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMxNTI3-----Wayne-Snider"},"E32038":{"name_f":"Eileen","name_l":"Temprosa","email":"eileen.temprosa@apsva.us","apsid":"E32038","classification":["Instructional Staff"],"title":["Teacher"],"grade":["6","7","8"],"course":["Case Carrier","Math 7","7th Grade HR"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyMDM4-----Eileen-Temprosa"},"E32095":{"name_f":"Robert","name_l":"Griffiths","email":"robert.griffiths@apsva.us","apsid":"E32095","grade":["6"],"title":["Teacher"],"classification":["Instructional Staff"],"house":["Dolphins"],"site":["Jefferson MS"],"course":["US History to Present","6th Grade HR"],"category":"Instructional Staff","team":"Dolphins, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyMDk1-----Robert-Griffiths"},"E32152":{"name_f":"Kristina","name_l":"McQuay","email":"kristina.mcquay@apsva.us","apsid":"E32152","title":["Instructional Assistant"],"site":["Jefferson MS"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyMTUy-----Kristina-McQuay"},"E32175":{"name_f":"Olivia","name_l":"Holloway","email":"olivia.holloway@apsva.us","apsid":"E32175","site":["Jefferson MS"],"grade":["6","7","8"],"title":["Teacher"],"course":["ELD 1 Social Studies","ELD 1-2 U.S. History to Present","6th Grade HR","ELD Case Manager"],"house":["Dolphins"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"Dolphins, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyMTc1-----Olivia-Holloway"},"E32182":{"name_f":"Joshua","name_l":"Chung","email":"joshua.chung@apsva.us","apsid":"E32182","title":["Instructional Assistant"],"classification":["Instructional Staff"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyMTgy-----Joshua-Chung"},"E32184":{"name_f":"Miguel","name_l":"Perez-Calleja","email":"miguel.perez@apsva.us","apsid":"E32184","site":["Jefferson MS"],"course":["ELD 2 Reading","ELD Case Manager","7th Grade HR","ELD 2 English","ELD 3 English"],"grade":["6","7","8"],"house":["Dragons"],"classification":["Instructional Staff"],"title":["Teacher"],"category":"Instructional Staff","team":"Dragons, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyMTg0-----Miguel-Perez-Calleja"},"E32230":{"name_f":"Andrea","name_l":"Menza","email":"andrea.menza@apsva.us","apsid":"E32230","title":["Teacher"],"course":["Grade 6 Health and Physical Education ","Grade 8 Health and Physical Education ","6th Grade HR"],"grade":["6","8"],"house":["Owls"],"classification":["Instructional Staff"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Owls, 6, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyMjMw-----Andrea-Menza"},"E32416":{"name_f":"Alicia","name_l":"Cofield","email":"alicia.cofield@apsva.us","apsid":"E32416","title":["Staff"],"site":["Jefferson MS"],"classification":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNDE2-----Alicia-Cofield"},"E32417":{"name_f":"Nicci","name_l":"Luu","email":"nicci.luu@apsva.us","apsid":"E32417","classification":["Instructional Staff"],"site":["Jefferson MS"],"title":["Instructional Assistant"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNDE3-----Nicci-Luu"},"E32421":{"name_f":"Edith","name_l":"Benitez Velasquez","email":"edith.benitez@apsva.us","apsid":"E32421","site":["Jefferson MS"],"classification":["Staff"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNDIx-----Edith-Benitez Velasquez"},"E32603":{"name_f":"Kelley","name_l":"Perry","email":"kelley.perry@apsva.us","apsid":"E32603","classification":["Instructional Staff"],"site":["Jefferson MS"],"grade":["6","7","8"],"house":["Dragons"],"course":["7th Grade HR","Life Science","Case Carrier"],"title":["Teacher"],"category":"Instructional Staff","team":"Dragons, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNjAz-----Kelley-Perry"},"E32639":{"name_f":"Claire","name_l":"LeBovidge","email":"claire.lebovidge@apsva.us","apsid":"E32639","site":["Jefferson MS"],"course":["6th Grade HR","Grade 6 Science "],"house":["Stingrays"],"grade":["6"],"classification":["Instructional Staff"],"title":["Teacher"],"category":"Instructional Staff","team":"Stingrays, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNjM5-----Claire-LeBovidge"},"E32647":{"name_f":"Verne","name_l":"Hyde","email":"verne.hyde@apsva.us","apsid":"E32647","site":["Jefferson MS"],"grade":["6","7","8"],"classification":["Instructional Staff"],"course":["Homeroom Middle School"],"title":["Teacher"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNjQ3-----Verne-Hyde"},"E32674":{"name_f":"Tiffanie","name_l":"Otto","email":"tiffanie.otto@apsva.us","apsid":"E32674","grade":["6"],"house":["Stingrays"],"classification":["Instructional Staff"],"course":["Grade 6 Reading"],"title":["Teacher"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Stingrays, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNjc0-----Tiffanie-Otto"},"E32705":{"name_f":"Krystal","name_l":"Wilson","email":"krystal.wilson@apsva.us","apsid":"E32705","grade":["6","7","8"],"course":["Case Carrier","Reading 7"],"classification":["Instructional Staff"],"title":["Teacher"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNzA1-----Krystal-Wilson"},"E32744":{"name_f":"Donna","name_l":"Seabrook","email":"donna.seabrook@apsva.us","apsid":"E32744","site":["Jefferson MS"],"title":["Instructional Assistant"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNzQ0-----Donna-Seabrook"},"E32754":{"name_f":"Sabrina","name_l":"Suarez","email":"sabrina.suarez@apsva.us","apsid":"E32754","classification":["Staff"],"site":["Jefferson MS"],"title":["Administrative Assistant"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNzU0-----Sabrina-Suarez"},"E32796":{"name_f":"Chanai","name_l":"Brewster","email":"chanai.brewster@apsva.us","apsid":"E32796","site":["Jefferson MS"],"classification":["Instructional Staff"],"course":["American Sign Language I ","American Sign Language II ","Introduction to American Sign Language","7th Grade HR"],"grade":["6","7","8"],"house":["Monarchs"],"title":["Teacher"],"category":"Instructional Staff","team":"Monarchs, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNzk2-----Chanai-Brewster"},"E32797":{"name_f":"Shaina","name_l":"Womack","email":"shaina.womack@apsva.us","apsid":"E32797","site":["Jefferson MS"],"grade":["6","7","8"],"house":["Dolphins"],"classification":["Instructional Staff"],"course":["English 6","6th Grade HR","Case Carrier"],"title":["Teacher"],"category":"Instructional Staff","team":"Dolphins, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyNzk3-----Shaina-Womack"},"E32830":{"name_f":"Benedicta","name_l":"Tugbeh","email":"benedicta.tugbeh@apsva.us","apsid":"E32830","site":["Jefferson MS"],"grade":["6","7","8"],"course":["8th Grade HR","World Geography","Case Carrier"],"classification":["Instructional Staff"],"title":["Teacher"],"house":["Gators"],"category":"Instructional Staff","team":"Gators, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyODMw-----Benedicta-Tugbeh"},"E32841":{"name_f":"Kevin","name_l":"Moore","email":"kevin.moore@apsva.us","apsid":"E32841","course":["Civics and Economics","Case Carrier","7th Grade HR"],"grade":["6","7","8"],"house":["Monarchs"],"title":["Teacher"],"classification":["Instructional Staff"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Monarchs, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyODQx-----Kevin-Moore"},"E32871":{"name_f":"Maggie","name_l":"Luu","email":"maggie.luu@apsva.us","apsid":"E32871","title":["Administrative Assistant"],"site":["Jefferson MS"],"classification":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyODcx-----Maggie-Luu"},"E32980":{"name_f":"Garrett","name_l":"Gibson","email":"garrett.gibson@apsva.us","apsid":"E32980","classification":["Staff"],"site":["Jefferson MS"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMyOTgw-----Garrett-Gibson"},"E33065":{"name_f":"Saliha","name_l":"Bahfid","email":"saliha.bahfid@apsva.us","apsid":"E33065","site":["Jefferson MS"],"classification":["Staff"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMzMDY1-----Saliha-Bahfid"},"E33152":{"name_f":"Sophia","name_l":"Nowlin","email":"sophia.nowlin@apsva.us","apsid":"E33152","classification":["Staff"],"site":["Jefferson MS"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMzMTUy-----Sophia-Nowlin"},"E33157":{"name_f":"Kelley","name_l":"Presley","email":"kelley.presley@apsva.us","apsid":"E33157","title":["Teacher"],"classification":["Instructional Staff"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMzMTU3-----Kelley-Presley"},"E33314":{"name_f":"Leeco","name_l":"Gutierrez","email":"leeco.gutierrez@apsva.us","apsid":"E33314","title":["Instructional Assistant"],"classification":["Instructional Staff"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMzMzE0-----Leeco-Gutierrez"},"E33512":{"name_f":"Edward","name_l":"Dillard","email":"edward.dillard@apsva.us","apsid":"E33512","classification":["Facilities Staff"],"title":["Maintenance Supervisor"],"site":["Jefferson MS"],"category":"Facilities Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMzNTEy-----Edward-Dillard"},"E3364":{"name_f":"Jeremy","name_l":"Siegel","email":"jeremy.siegel@apsva.us","apsid":"E3364","classification":["Staff"],"title":["Coordinator"],"site":["Jefferson MS"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTMzNjQ=-----Jeremy-Siegel"},"E3713":{"name_f":"Mary","name_l":"Brown","email":"mary.brown@apsva.us","apsid":"E3713","house":["Owls"],"site":["Jefferson MS"],"course":["Grade 6 English ","6th Grade HR"],"classification":["Instructional Staff"],"grade":["6"],"title":["Teacher"],"category":"Instructional Staff","team":"Owls, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTM3MTM=-----Mary-Brown"},"E4193":{"name_f":"Nicci","name_l":"Gibson Williams","email":"nicci.williams@apsva.us","apsid":"E4193","grade":["6","7","8"],"site":["Jefferson MS"],"course":["Math 6","6th Grade HR","Case Carrier"],"classification":["Instructional Staff"],"title":["Teacher"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTQxOTM=-----Nicci-Gibson Williams"},"E4554":{"name_f":"Jason","name_l":"Busby","email":"jason.busby@apsva.us","apsid":"E4554","title":["Teacher"],"course":["8th Grade HR","Algebra I, Intensified ","Pre-Algebra for 8th Graders "],"grade":["6","7","8"],"site":["Jefferson MS"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTQ1NTQ=-----Jason-Busby"},"E4991":{"name_f":"Emily","name_l":"Shepardson","email":"emily.shepardson@apsva.us","apsid":"E4991","classification":["Instructional Staff"],"site":["Jefferson MS"],"house":["Stingrays"],"grade":["6","7","8"],"course":["Visual Arts","Visual Arts II, Semester","Visual Arts I","6th Grade HR"],"title":["Teacher"],"category":"Instructional Staff","team":"Stingrays, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTQ5OTE=-----Emily-Shepardson"},"E5034":{"name_f":"Kimberly","name_l":"Miller","email":"kimberly.miller@apsva.us","apsid":"E5034","course":["6th Grade HR","Grade 6 Science "],"classification":["Instructional Staff"],"grade":["6"],"house":["Dolphins"],"site":["Jefferson MS"],"title":["Teacher"],"category":"Instructional Staff","team":"Dolphins, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTUwMzQ=-----Kimberly-Miller"},"E5394":{"name_f":"Katarzyna","name_l":"Polek-Craven","email":"katarzyna.craven@apsva.us","apsid":"E5394","title":["Teacher"],"classification":["Instructional Staff"],"grade":["6","7","8"],"house":["Dragons"],"course":["English 7","7th Grade HR","Case Carrier"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Dragons, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTUzOTQ=-----Katarzyna-Polek-Craven"},"E5594":{"name_f":"Kirsten","name_l":"Wall","email":"kirsten.wall@apsva.us","apsid":"E5594","classification":["Staff","Instructional Staff"],"title":["Staff","Teacher"],"site":["Jefferson MS"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTU1OTQ=-----Kirsten-Wall"},"E6346":{"name_f":"Jennifer","name_l":"Bogdan","email":"jennifer.bogdan@apsva.us","apsid":"E6346","course":["7th Grade HR","ELD Case Manager","Grade 8 Physical Science ","Grade 7 Life Science ","ELD 1 Science","Grade 6 Science "],"grade":["6","7","8"],"house":["Monarchs"],"classification":["Instructional Staff"],"title":["Teacher"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"Monarchs, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTYzNDY=-----Jennifer-Bogdan"},"E6403":{"name_f":"Laura","name_l":"Bannach","email":"laura.bannach@apsva.us","apsid":"E6403","site":["Jefferson MS"],"course":["Science 6","6th Grade HR","Case Carrier"],"grade":["6","7","8"],"classification":["Instructional Staff"],"house":["Dolphins"],"title":["Teacher"],"category":"Instructional Staff","team":"Dolphins, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTY0MDM=-----Laura-Bannach"},"E6623":{"name_f":"Alexandra","name_l":"Workman","email":"alexandra.workman@apsva.us","apsid":"E6623","course":["Grade 6 Science ","6th Grade HR"],"grade":["6"],"house":["Owls"],"site":["Jefferson MS"],"title":["Teacher"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"Owls, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTY2MjM=-----Alexandra-Workman"},"E7008":{"name_f":"Renee","name_l":"Sidberry","email":"renee.sidberry@apsva.us","apsid":"E7008","classification":["Staff"],"site":["Jefferson MS"],"title":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTcwMDg=-----Renee-Sidberry"},"E7125":{"name_f":"Pidor","name_l":"Chea","email":"pidor.chea@apsva.us","apsid":"E7125","site":["Jefferson MS"],"title":["Manager"],"classification":["Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTcxMjU=-----Pidor-Chea"},"E7338":{"name_f":"Katherine","name_l":"Zientara","email":"katherine.zientara@apsva.us","apsid":"E7338","site":["Jefferson MS"],"title":["Instructional Assistant"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTczMzg=-----Katherine-Zientara"},"E779":{"name_f":"Tracey","name_l":"Shepardson","email":"tracey.shepardson@apsva.us","apsid":"E779","title":["Teacher"],"classification":["Instructional Staff"],"course":["6th Grade HR","Grade 6 English "],"site":["Jefferson MS"],"house":["Dolphins"],"grade":["6"],"category":"Instructional Staff","team":"Dolphins, 6","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTc3OQ==-----Tracey-Shepardson"},"E7898":{"name_f":"C","name_l":"Tangchittsumran","email":"c.tangchittsumran@apsva.us","apsid":"E7898","title":["Teacher"],"course":["8th Grade HR","Advanced Band, Full Year ","Intermediate Band","Beginning Band"],"grade":["6","7","8"],"house":["Eagles"],"site":["Jefferson MS"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTc4OTg=-----C-Tangchittsumran"},"E7986":{"name_f":"Nataki","name_l":"Green","email":"nataki.green@apsva.us","apsid":"E7986","site":["Jefferson MS"],"title":["Instructional Assistant","Staff"],"classification":["Staff","Instructional Staff"],"category":"Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTc5ODY=-----Nataki-Green"},"E8354":{"name_f":"Cassidy","name_l":"Nolen","email":"cassidy.nolen@apsva.us","apsid":"E8354","grade":["6","7","8"],"course":["7th Grade HR","Technological Systems, Semester","Inventions and Innovations ","Technology Education"],"site":["Jefferson MS"],"classification":["Instructional Staff"],"house":["Dragons"],"title":["Teacher"],"category":"Instructional Staff","team":"Dragons, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTgzNTQ=-----Cassidy-Nolen"},"E8576":{"name_f":"Deisy","name_l":"Molina Fuentes","email":"deisy.villatoro@apsva.us","apsid":"E8576","title":["Instructional Assistant"],"site":["Jefferson MS"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTg1NzY=-----Deisy-Molina Fuentes"},"E9346":{"name_f":"Maritza","name_l":"Argueta","email":"maritza.argueta@apsva.us","apsid":"E9346","title":["Instructional Assistant"],"site":["Jefferson MS"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTkzNDY=-----Maritza-Argueta"},"E9600":{"name_f":"Cynthia","name_l":"Pelham","email":"cynthia.pelham@apsva.us","apsid":"E9600","title":["Instructional Assistant"],"classification":["Instructional Staff"],"site":["Jefferson MS"],"category":"Instructional Staff","team":"","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTk2MDA=-----Cynthia-Pelham"},"E994":{"name_f":"Heidi","name_l":"Neunder","email":"heidi.neunder@apsva.us","apsid":"E994","title":["Teacher"],"course":["8th Grade HR","Social Skills","Instructional Studies"],"grade":["6","7","8"],"site":["Jefferson MS"],"house":["Eagles"],"classification":["Instructional Staff"],"category":"Instructional Staff","team":"Eagles, 6, 7, 8","page_url":"https:\/\/jefferson.apsva.us\/staff-directory-page\/RTk5NA==-----Heidi-Neunder"}};
        </script>

        <script id="template-controls" type="text/html">
            <div class="row">

                <h6 class="columns  small-16 medium-2 large-1">
                    Search
                </h6>

                <div class="columns small-16 medium-3">
                    <label for="filter_fname" class="show-for-sr">Filter by First Name</label>
                    <input type="text" name="filter_fname" id="filter_fname" placeholder="By First Name"/>
                </div>

                <div class="columns small-16 medium-3">
                    <label for="filter_lname" class="show-for-sr">Filter by Last Name</label>
                    <input type="text" name="filter_lname" id="filter_lname" placeholder="By Last Name"/>
                </div>

                                <div class="columns small-16 medium-3">
                    <label for="filter_team" class="show-for-sr">Filter by Grade or Team</label>
                    <input type="text" name="filter_team" id="filter_team" placeholder="By Grade or Team"/>
                </div>

                                <div class="columns small-16 medium-3">
                    <label for="filter_courses" class="show-for-sr">Filter by Course</label>
                    <input type="text" name="filter_courses" id="filter_courses" placeholder="By Course"/>
                </div>
                                <div class="columns small-16 medium-2 right">
                    <label for="filter_category" class="show-for-sr">Filter by Category</label>
                    <select id="filter_category" name="filter_category">
                        <option value="">Categories</option>
                        <%= data.category_options %>
                    </select>
                </div>
            </div>

        </script>

        <script id="template-content" type="text/html">
            <header class="row show-for-medium-up">
                <div class="columns small-3">Name</div>
                <div class="columns small-3">Title/Position</div>
                                <div class="columns small-3">Grade or Team</div>
                                                <div class="columns small-3">Courses</div>
                                <div class="columns small-4 end">Email Address</div>
            </header>
            <div class="listings">
                <% if (data.listings !== "") { %>
                <%= data.listings %>
                <% } else { %>
                <h1>No listings found.</h1>
                <% } %>
            </div>
            <footer>
                <%= data.pagination %>
            </footer>
        </script>

        <script id="template-content-row" type="text/html">
            <div class="row">
                <div class="name"><a href="<%= data.page_url %>"><%= data.name_l %>, <%= data.name_f %></a></div>
                <div class="title"><%= data.title %></div>
                                <div class="team"> <%= data.team %></div>
                                                <% if ("undefined" != typeof data.course) { %>
                    <div class="courses"> <%= data.course.join(', ') %></div>
                <% } else { %>
                    <div class="courses"></div>
                <% } %>
                                <div class="email"><a class="notranslate" href="mailto:<%= data.email %>"><%= data.email %></a></div>
            </div>
        </script>

        				</article>
										<div class="materiell-social-share">
	<a href='https://www.facebook.com/share.php?u=http://jefferson.apsva.us/staff-directory/&title=Staff+Directory' title='Facebook' title='facebook' rel='nofollow' target='_blank' class='materiell-share-icon facebook'><img src='https://jefferson.apsva.us/wp-content/plugins/materiell-social-share/public/img/facebook.png' width='' height='' alt='Facebook'></a><a href='http://twitter.com/intent/tweet?text=Staff+Directory+http://jefferson.apsva.us/staff-directory/' title='Twitter' title='twitter' rel='nofollow' target='_blank' class='materiell-share-icon twitter'><img src='https://jefferson.apsva.us/wp-content/plugins/materiell-social-share/public/img/twitter.png' width='' height='' alt='Twitter'></a><a href='http://www.linkedin.com/shareArticle?mini=true&url=Staff+Directory&title=http://jefferson.apsva.us/staff-directory/&source=https://jefferson.apsva.us' title='Linkedin' title='linkedin' rel='nofollow' target='_blank' class='materiell-share-icon linkedin'><img src='https://jefferson.apsva.us/wp-content/plugins/materiell-social-share/public/img/linkedin.png' width='' height='' alt='Linkedin'></a><a href='https://plus.google.com/share?url=http://jefferson.apsva.us/staff-directory/' title='Google Plus' title='gplus' rel='nofollow' target='_blank' class='materiell-share-icon gplus'><img src='https://jefferson.apsva.us/wp-content/plugins/materiell-social-share/public/img/gplus.png' width='' height='' alt='Google Plus'></a><a href='mailto:?subject=Staff Directory&body=http://jefferson.apsva.us/staff-directory/' title='Email' title='email' rel='nofollow' target='_blank' class='materiell-share-icon email'><img src='https://jefferson.apsva.us/wp-content/plugins/materiell-social-share/public/img/email.png' width='' height='' alt='Email'></a><a href='https://plus.google.com/share?url=http://jefferson.apsva.us/staff-directory/' title='Print' title='print_page' rel='nofollow' target='_blank' class='materiell-share-icon print_page'><img src='https://jefferson.apsva.us/wp-content/plugins/materiell-social-share/public/img/print.png' width='' height='' alt='Print'></a></div>
					</div>
	</div>

</section>
</div>
<div class="footer-container clearfix" role="navigation">

	<footer>

				<div class="top-widgets" data-equalizer>

			<div class="logo-box">
				<a href="http://apsva.us/">

					<img src="https://jefferson.apsva.us/wp-content/themes/apsmain/assets/img/svg/APS-FooterLogo.svg" title="Arlington Public Schools" alt="Arlington Public Schools">
				</a>
			</div>
			<article id="text-1" class="widget widget_text" data-equalizer-watch><h6>Thomas Jefferson Middle School</h6>			<div class="textwidget"> Keisha Boggan, Principal<br />
125 South Old Glebe Road<br />
Arlington, VA 22204<br />
703-228-5900<br />
Fax: 703-979-3744 </div>
		</article><article id="text-2" class="widget widget_text" data-equalizer-watch><h6>Questions or Feedback?</h6>			<div class="textwidget"><a href='mailto:webmaster@apsva.us'>webmaster@apsva.us</a></div>
		</article>			<article class="widget" data-equalizer-watch>
				<h6>Let's Get Social</h6>
				<ul class="social">
					<li><a href="https://www.facebook.com/ArlingtonPublicSchools" aria-label="Facebook Social Media Link"><i role="presentation" class="fa fa-facebook"></i></a></li>
					<li><a href="https://twitter.com/intent/user?screen_name=JeffersonIBMYP" aria-label="Twitter Social Media Link"><i role="presentation" class="fa fa-twitter"></i></a></li>
					<li><a href="https://www.youtube.com/channel/UCcLE6BjDzaoKXIaaO9SOXSw" aria-label="Youtube Social Media Link"><i role="presentation" class="fa fa-youtube-play"></i></a></li>
					<li><a href="https://www.facebook.com/ArlingtonPublicSchools" aria-label="Instagram Social Media Link"><i role="presentation" class="fa fa-instagram"></i></a></li>
				</ul>
			</article>
			<div id="top-widgets" class="hidden"></div>
		</div>

		<div class="policy-area">
			<article id="text-4" class="footer-bottom-widgets widget widget_text">			<div class="textwidget">Arlington Public Schools prohibits discrimination on the basis of race, national origin, creed, color, religion, gender, age, economic status, sexual orientation, marital status, genetic information, gender identity or expression, and/or disability.
This policy provides equal access to courses and programs, counseling services, physical education and athletics, vocational education, instructional materials and extra-curricular activities.
Report violations of this policy to the Assistant Superintendent for Administrative Services, 703-228-6008, or the Assistant Superintendent for Human Resources, 703-228-6110.
<br>
<br>
These webpages may contain links to websites that are outside of the Arlington Public Schools network.
APS does not control the content or relevancy of these outside sites.</div>
		</article>		</div>
		<div class="bottom-info">

			<div class="columns small-16 medium-8">
				<ul class="inline-list">
					<li class="copyright " role="contentinfo">
						&copy; 2022 Arlington Public Schools
					</li>
																<li><a href="https://go.boarddocs.com/vsba/arlington/Board.nsf/files/AZ6Q5M67A5C3/$file/I-9.2.5.1%20PIP-1%20Acceptable%20Use%20of%20Networked%20Information%20Resources.pdf">Terms of Use</a></li>
											<li><a href="https://go.boarddocs.com/vsba/arlington/Board.nsf/files/AZ54WR0D94CF/$file/K-2.5%20PIP-1%20Internet%20Privacy.pdf">Privacy Policy</a></li>
																<li><a href="https://jefferson.apsva.us/wp-login.php?normal">Direct WordPress Login</a></li>
									</ul>
			</div>
			<div class="columns small-16 medium-8 right text-right">
				<a href="https://materiell.com" alt="Education Web Design | Materiell">
					WordPress Design for Education |
					<img class="materiell-logo"
						 src="https://jefferson.apsva.us/wp-content/themes/apsmain/assets/img/svg/materiell-mark.svg"
						 alt="WordPress Design for Education | Materiell"/>
					Materiell
				</a>
			</div>
		</div>

	</footer>
</div>


</div>
<a class="exit-off-canvas"></a>
<script type='text/javascript' src='https://jefferson.apsva.us/wp-content/mu-plugins/mat-externalLinks/externalLinks.js?v=1.0.04' id='mat_forceExternalLinks-js'></script>
<script type='text/javascript' src='https://jefferson.apsva.us/wp-includes/js/underscore.min.js' id='underscore-js'></script>
<script type='text/javascript' src='https://jefferson.apsva.us/wp-content/themes/apsmain/js/foundation.js?v=1.0.19' id='foundation-js'></script>
        <script>
            window.alert_blog_url = 'www.apsva.us';
            window.alert_blog_current = 22;
        </script>
                <script type="text/javascript">
            /*<![CDATA[*/
            (function () {
                    var sz = document.createElement('script');
                    sz.type = 'text/javascript';
                    sz.async = true;
                    sz.src = '//siteimproveanalytics.com/js/siteanalyze_6005447.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(sz, s);
                })();
            /*]]>*/
        </script>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-81204003-21', 'auto');
  ga('send', 'pageview');

</script><div style="display:none;" id="gt-rm-txt">Read More</div>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"afb86e4eb8","applicationID":"59460826","transactionName":"YQAEbEdUXkZVBRYKV1hKJ1tBXF9bGxYDBF0bFhJZU1MdUV0UBwBMWRcf","queueTime":0,"applicationTime":465,"atts":"TUcHGg9OTUg=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
"""
#maybe I'm not supposed to paste it in here

from bs4 import BeautifulSoup
soup = BeautifulSoup(html_doc, 'html.parser')

print(soup.find_all('aps_directory_data'))

