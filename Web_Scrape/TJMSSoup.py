from bs4 import BeautifulSoup

import requests

#https://www.geeksforgeeks.org/beautifulsoup-scraping-link-from-html/

def getHTML(url):
    response = requests.get(url)
    return response.text

urls = ["https://www.apsva.us/staff-directory/",
        "https://abingdon.apsva.us/staff-directory/",
        "https://achs.apsva.us/staff-directory/"
        ]
jsons = []

for u in urls:
    html = getHTML(u)
    print(html)
    soup = BeautifulSoup(html, 'html.parser')
