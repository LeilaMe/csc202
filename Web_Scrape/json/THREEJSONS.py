from TJMS import aps_directory_data
tjms = aps_directory_data

from ABINGDON import aps_directory_data
abingdon = aps_directory_data

from DISTRICTDIRECTORY import aps_directory_data
districtdirectory = aps_directory_data

jsonlist = [abingdon, districtdirectory, tjms]
jsonNameStrings = ["abingdon","districtdirectory","tjms"]
count = 0

keys_with_strings = ['name_f', 'name_l', 'email', 'apsid','team']
keys_with_lists = ['title', 'course','site']
f = open("fruitsandcandy.txt", "w")

f.write("dataset,")
for i in keys_with_strings:
    f.write(i + ',')
for i in keys_with_lists:
    if i == keys_with_lists[-1]:
        f.write(i)
    else:
        f.write(i + ',')
f.write("\n")

for json in jsonlist:
    for thing in json:
        f.write(jsonNameStrings[count] + ',')
        for s in keys_with_strings:
            if s in json[thing]:
                no_commas = json[thing][s].split(',')
                for word in no_commas:
                    if word == no_commas[-1]:
                        f.write(word)
                    else:
                        f.write(word + '|')
            else:
                f.write("none")
            f.write(",")
        for l in keys_with_lists:
            if l in json[thing]:
                for item in json[thing][l]:
                    no_commas = item.split(',')
                    for word in no_commas:
                        f.write(word)
                    if item != json[thing][l][-1]:
                        f.write("|")
            else:
                f.write("None")
            if l != keys_with_lists[-1]:
                f.write(",")
        f.write("\n")
    count += 1

f.close()
