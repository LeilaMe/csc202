my_dictionary = {
        "key1": "thing", 
        "key2": ["one", "two", "three"], 
        "key3": {
            "key4": "a", 
            "key5": ["b", "c"]
            }
        }

#print(my_dictionary)
#print(my_dictionary['key1'])
#for i in my_dictionary['key2']:
#    print(i)
#print(my_dictionary['key3']['key4'])
#for i in my_dictionary['key3']['key5']:
#    print(i)
for i in my_dictionary:
    for e in i:
        print(e)
