fruits = {
        "banana":{
            "color":"yellow","friends":["bob","susan"],"name": "ms. banana, jr."
            },
        "orange":{
            "color":"orange","friends":["bob","kelly"],"job":"window wiper"
            }
        }
candy = {
        "lemondrop":{
            "color":"yellow","friends":["susan"],"name":"sally","job":"paper puncher, cat wrestler"
            },
        "lollipop":{
            "color":"rainbow","friends":[],"name":"swirl","job":""
            }
        }

jsonlist = [fruits, candy]

keys_with_strings = ['color','name','job']
keys_with_lists = ['friends','no']
f = open("fruitsandcandy.txt", "w")

for i in keys_with_strings:
    f.write(i + ',')
for i in keys_with_lists:
    f.write(i + ',')
f.write("\n")

for json in jsonlist:
    for thing in json:
        for s in keys_with_strings:
            if s in json[thing]:
                no_commas = json[thing][s].split(',')
                for word in no_commas:
                    if word == no_commas[-1]:
                        f.write(word)
                    else:
                        f.write(word + '|')
            else:
                f.write("none")
            f.write(",")
        for l in keys_with_lists:
            if l in json[thing]:
                for item in json[thing][l]:
                    no_commas = item.split(',')
                    for word in no_commas:
                        f.write(word)
                    if item != json[thing][l][-1]:
                        f.write("|")
            else:
                f.write("None")
            f.write(",")
        f.write("\n")

f.close()
