#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

int main() {
    char word[50];
    char key[20];
    cin >> key;
    cin >> word;
    int keylen = strlen(key);
    char output;

    while(strcmp(word, "-1") != 0){
        int i, keyindex = 0;
        for(i=0; i< strlen(word); i++){
            output = 'a'+((word[i]+key[keyindex]-'a')%'a'%26); 
            cout << output;
            if(keyindex < keylen-1){
                keyindex++;
            }
            else{
                keyindex = 0;
            }
        }
        cout << "\n";
        cin >> word;
    }
    return 0;
}
