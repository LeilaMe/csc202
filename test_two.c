#include <stdio.h>
#include "minunit.h"
#include "two.c"

int tests_run = 0;

int a[] = {1,3};
int b[] = {8,0};

static char * test_two_01() {
	mu_assert("error, two([1,3],2) != 2", two(a,2) == 2);
	return 0;
}

static char * test_two_02() {
        mu_assert("error, two([8,0],2) != 4", two(b,2) == 4);
        return 0;
}

static char * all_tests() {
	mu_run_test(test_two_01);
	mu_run_test(test_two_02);
	return 0;
}

int main(int argc, char **argv) {
	char *result = all_tests();
	if (result != 0) {
		printf("%s\n", result);
	}
	else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);
	return result != 0;
}
